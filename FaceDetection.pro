QT      += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ImageTransformation
TEMPLATE = app

#DEFINES += USE_OPENCV2
DEFINES += USE_OPENCV3

CONFIG += use_opencv3

# OPENCV directory OPENCV2: C:\Bin\OpenCV-2.4.10\opencv\build
#                  OPENCV3: C:/Bin/OpenCV-3.0.0/opencv/build
OPENCV_PATH = $$(OPENCV_DIR)

# INTEL TBB installation directory
TBB_PATH = $$(TBB_DIR)

# OPENCV include directory
INCLUDEPATH += $$OPENCV_PATH/mingw_4.9.1_32bit_release/install/include

SOURCES += main.cpp\
           cmainwindow.cpp \
           cqtopencvviewergl.cpp \
           cqpicture.cpp \
           cqfilemanager.cpp \
           cqpicturescript.cpp \
           cqsettings.cpp

HEADERS += cmainwindow.h \
           cqtopencvviewergl.h \
           cqpicture.h \
           cqfilemanager.h \
           cqpicturescript.h \
           cqsettings.h

FORMS   += cmainwindow.ui

win32 {
    message("Using win32 configuration")

    # Insert logo
    RC_ICONS += /images/logo.ico

    # OPENCV library directory
    # Note: to be changed if using MinGW or VS and depending on the x86 or x64 version used
    #LIBS_PATH = "$$OPENCV_PATH/x86/mingw_4.9.1_32bit_tbb_vtk/lib"

    CONFIG(debug, debug|release) {
        LIBS_PATH = "$$OPENCV_PATH/mingw_4.9.1_32bit_debug/lib"

        use_opencv3 {
            LIBS     += -L$$LIBS_PATH \
                        -lopencv_core300d \
                        -lopencv_highgui300d \
                        -lopencv_objdetect300d \
                        -lopencv_imgproc300d \
                        -lopencv_videoio300d \
                        -lopencv_imgcodecs300d
        } else {
            LIBS     += -L$$LIBS_PATH \
                        -lopencv_core2410d \
                        -lopencv_highgui2410d \
                        -lopencv_objdetect2410d \
                        -lopencv_imgproc2410d
        }
    }

    CONFIG(release, debug|release) {
    LIBS_PATH = "$$OPENCV_PATH/mingw_4.9.1_32bit_release/lib"

        use_opencv3 {
            LIBS     += -L$$LIBS_PATH \
                        -lopencv_core300 \
                        -lopencv_highgui300 \
                        -lopencv_objdetect300 \
                        -lopencv_imgproc300 \
                        -lopencv_videoio300 \
                        -lopencv_imgcodecs300
        } else {
            LIBS     += -L$$LIBS_PATH \
                        -lopencv_core2410 \
                        -lopencv_highgui2410 \
                        -lopencv_objdetect2410 \
                        -lopencv_imgproc2410
        }
    }

    LIBS += $$TBB_PATH/tbb.dll
}

unix {
    message("Using unix configuration")

    OPENCV_PATH = /usr/local/opencv2/

    LIBS     += -L$$LIBS_PATH \
                -lopencv_core \
                -lopencv_highgui \
                -lopencv_objdetect \
                -lopencv_imgproc
}

INCLUDEPATH += $$OPENCV_PATH/modules/core/include/ \      # core module
               $$OPENCV_PATH/modules/highgui/include/ \   # highgui module
               $$OPENCV_PATH/modules/objdetect/include/ \ # obj_detect module
               $$OPENCV_PATH/modules/imgproc/include      # image processing module

message("OpenCV path: $$OPENCV_PATH")
message("TBB path: $$TBB_PATH")
message("Includes path: $$INCLUDEPATH")
message("Libraries: $$LIBS")

RESOURCES += \
    resources.qrc
