/********************************************************************************
** Form generated from reading UI file 'cmainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.4.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CMAINWINDOW_H
#define UI_CMAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "cqtopencvviewergl.h"

QT_BEGIN_NAMESPACE

class Ui_CMainWindow
{
public:
    QAction *actionStart;
    QAction *actionStop;
    QAction *actionReset;
    QAction *actionLoad_Image;
    QAction *actionOpen_Howto;
    QAction *actionSave_processed_image;
    QAction *actionLoad_and_Execute_script;
    QAction *actionWatersheed;
    QWidget *centralWidget;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout;
    QCheckBox *checkBox_fullFace;
    QCheckBox *checkBox_eyes;
    QCheckBox *checkBox_nose;
    QCheckBox *checkBox_mouth;
    QPushButton *pushButton_applyThreshold;
    QComboBox *comboBox_thresholdType;
    QLabel *label_thresholdType;
    QLabel *label_thresholdValue;
    QLineEdit *lineEdit_thresholdValue;
    QLabel *label_thresholdMax;
    QLineEdit *lineEdit_thresholdMax;
    QFrame *line;
    QLabel *label_rightImage;
    QLabel *label_leftImage;
    QFrame *line_2;
    QPushButton *pushButton_applyGreyScale;
    QFrame *line_3;
    QFrame *line_4;
    CQtOpenCVViewerGl *cameraWidget_history;
    QLabel *label_history;
    QSlider *verticalSlider_history;
    QPushButton *pushButton_applyMorphologyEx;
    QFrame *line_5;
    QLabel *label_morphologyExStructElemShape;
    QComboBox *comboBox_morphologyExStructElemShape;
    QLabel *label_morphologyExIterationNumber;
    QLineEdit *lineEdit_morphologyExIterationNumber;
    QComboBox *comboBox_morphologyExType;
    QLabel *label_morphologyExType_2;
    QLineEdit *lineEdit_morphologyExStructElemSize;
    QLabel *label_morphologyExStructElemSize;
    QPushButton *pushButton_applyDilate;
    QLabel *label_dilateIterationNumber;
    QLineEdit *lineEdit_dilateIterationNumber;
    QLabel *label_dilateStructElemSize;
    QLineEdit *lineEdit_dilateStructElemSize;
    QLabel *label_dilateStructElemShape;
    QComboBox *comboBox_dilateStructElemShape;
    QCheckBox *checkBox_thresholdOtsu;
    QLabel *label_colorConversionValue;
    QLineEdit *lineEdit_colorConversionValue;
    QPushButton *pushButton_applyDistanceTransform;
    QComboBox *comboBox_distanceTransformType;
    QLabel *label_distanceTransformType;
    QComboBox *comboBox_distanceTransformMaskSize;
    QLabel *label_distanceTransformMaskSize;
    QComboBox *comboBox_distanceTransformLabelType;
    QLabel *label_distanceTransformLabelType;
    QPushButton *pushButton_applySubtract;
    QPushButton *pushButton_applyConnectedComponents;
    QFrame *line_9;
    QFrame *line_11;
    QFrame *line_13;
    QFrame *frame_processedImage;
    CQtOpenCVViewerGl *cameraWidget_right;
    QFrame *frame;
    CQtOpenCVViewerGl *cameraWidget_left;
    QComboBox *comboBox_subtractSrc1;
    QComboBox *comboBox_subtractSrc2;
    QLabel *label_subtractMinus;
    QPushButton *pushButton_applyWatershed;
    QComboBox *comboBox_subtractResult;
    QLabel *label_subtractEqual;
    QLineEdit *lineEdit_subtractSrc1;
    QLineEdit *lineEdit_subtractSrc2;
    QComboBox *comboBox_connectedComponentsConnectivity;
    QComboBox *comboBox_connectedComponentsLType;
    QTextEdit *textEdit_detailedError;
    QPushButton *pushButton_getDetailedError;
    QComboBox *comboBox_applyWatershedMarkerImage;
    QLineEdit *lineEdit_colorConversionNumChannels;
    QLabel *label_colorConversionNumChannels;
    QLabel *label_colorConversionDepth;
    QComboBox *comboBox_colorConversionDepth;
    QFrame *line_6;
    QFrame *line_7;
    QMenuBar *menuBar;
    QMenu *menuCamera;
    QMenu *menuImage;
    QMenu *menuDocumentation;
    QMenu *menuOpenCv_3_Functions;
    QMenu *menuImage_Processing;
    QMenu *menuScript;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *CMainWindow)
    {
        if (CMainWindow->objectName().isEmpty())
            CMainWindow->setObjectName(QStringLiteral("CMainWindow"));
        CMainWindow->resize(1250, 681);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(CMainWindow->sizePolicy().hasHeightForWidth());
        CMainWindow->setSizePolicy(sizePolicy);
        actionStart = new QAction(CMainWindow);
        actionStart->setObjectName(QStringLiteral("actionStart"));
        actionStop = new QAction(CMainWindow);
        actionStop->setObjectName(QStringLiteral("actionStop"));
        actionReset = new QAction(CMainWindow);
        actionReset->setObjectName(QStringLiteral("actionReset"));
        actionLoad_Image = new QAction(CMainWindow);
        actionLoad_Image->setObjectName(QStringLiteral("actionLoad_Image"));
        actionOpen_Howto = new QAction(CMainWindow);
        actionOpen_Howto->setObjectName(QStringLiteral("actionOpen_Howto"));
        actionSave_processed_image = new QAction(CMainWindow);
        actionSave_processed_image->setObjectName(QStringLiteral("actionSave_processed_image"));
        actionLoad_and_Execute_script = new QAction(CMainWindow);
        actionLoad_and_Execute_script->setObjectName(QStringLiteral("actionLoad_and_Execute_script"));
        actionWatersheed = new QAction(CMainWindow);
        actionWatersheed->setObjectName(QStringLiteral("actionWatersheed"));
        centralWidget = new QWidget(CMainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        groupBox = new QGroupBox(centralWidget);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setGeometry(QRect(1120, 409, 120, 111));
        sizePolicy.setHeightForWidth(groupBox->sizePolicy().hasHeightForWidth());
        groupBox->setSizePolicy(sizePolicy);
        verticalLayout = new QVBoxLayout(groupBox);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        checkBox_fullFace = new QCheckBox(groupBox);
        checkBox_fullFace->setObjectName(QStringLiteral("checkBox_fullFace"));

        verticalLayout->addWidget(checkBox_fullFace);

        checkBox_eyes = new QCheckBox(groupBox);
        checkBox_eyes->setObjectName(QStringLiteral("checkBox_eyes"));
        checkBox_eyes->setEnabled(false);

        verticalLayout->addWidget(checkBox_eyes);

        checkBox_nose = new QCheckBox(groupBox);
        checkBox_nose->setObjectName(QStringLiteral("checkBox_nose"));
        checkBox_nose->setEnabled(false);

        verticalLayout->addWidget(checkBox_nose);

        checkBox_mouth = new QCheckBox(groupBox);
        checkBox_mouth->setObjectName(QStringLiteral("checkBox_mouth"));
        checkBox_mouth->setEnabled(false);

        verticalLayout->addWidget(checkBox_mouth);

        pushButton_applyThreshold = new QPushButton(centralWidget);
        pushButton_applyThreshold->setObjectName(QStringLiteral("pushButton_applyThreshold"));
        pushButton_applyThreshold->setGeometry(QRect(10, 401, 91, 51));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(pushButton_applyThreshold->sizePolicy().hasHeightForWidth());
        pushButton_applyThreshold->setSizePolicy(sizePolicy1);
        comboBox_thresholdType = new QComboBox(centralWidget);
        comboBox_thresholdType->setObjectName(QStringLiteral("comboBox_thresholdType"));
        comboBox_thresholdType->setGeometry(QRect(110, 427, 111, 22));
        label_thresholdType = new QLabel(centralWidget);
        label_thresholdType->setObjectName(QStringLiteral("label_thresholdType"));
        label_thresholdType->setGeometry(QRect(110, 402, 111, 16));
        label_thresholdType->setAlignment(Qt::AlignCenter);
        label_thresholdValue = new QLabel(centralWidget);
        label_thresholdValue->setObjectName(QStringLiteral("label_thresholdValue"));
        label_thresholdValue->setGeometry(QRect(230, 400, 81, 20));
        lineEdit_thresholdValue = new QLineEdit(centralWidget);
        lineEdit_thresholdValue->setObjectName(QStringLiteral("lineEdit_thresholdValue"));
        lineEdit_thresholdValue->setGeometry(QRect(238, 427, 61, 21));
        lineEdit_thresholdValue->setMaxLength(3);
        lineEdit_thresholdValue->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        label_thresholdMax = new QLabel(centralWidget);
        label_thresholdMax->setObjectName(QStringLiteral("label_thresholdMax"));
        label_thresholdMax->setGeometry(QRect(327, 400, 61, 20));
        lineEdit_thresholdMax = new QLineEdit(centralWidget);
        lineEdit_thresholdMax->setObjectName(QStringLiteral("lineEdit_thresholdMax"));
        lineEdit_thresholdMax->setGeometry(QRect(320, 427, 61, 21));
        lineEdit_thresholdMax->setMaxLength(3);
        lineEdit_thresholdMax->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        line = new QFrame(centralWidget);
        line->setObjectName(QStringLiteral("line"));
        line->setGeometry(QRect(10, 389, 1231, 16));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);
        label_rightImage = new QLabel(centralWidget);
        label_rightImage->setObjectName(QStringLiteral("label_rightImage"));
        label_rightImage->setGeometry(QRect(508, 370, 481, 20));
        label_rightImage->setAlignment(Qt::AlignCenter);
        label_leftImage = new QLabel(centralWidget);
        label_leftImage->setObjectName(QStringLiteral("label_leftImage"));
        label_leftImage->setGeometry(QRect(14, 370, 481, 20));
        label_leftImage->setAlignment(Qt::AlignCenter);
        line_2 = new QFrame(centralWidget);
        line_2->setObjectName(QStringLiteral("line_2"));
        line_2->setGeometry(QRect(10, 446, 541, 16));
        line_2->setFrameShape(QFrame::HLine);
        line_2->setFrameShadow(QFrame::Sunken);
        pushButton_applyGreyScale = new QPushButton(centralWidget);
        pushButton_applyGreyScale->setObjectName(QStringLiteral("pushButton_applyGreyScale"));
        pushButton_applyGreyScale->setGeometry(QRect(10, 456, 91, 51));
        sizePolicy1.setHeightForWidth(pushButton_applyGreyScale->sizePolicy().hasHeightForWidth());
        pushButton_applyGreyScale->setSizePolicy(sizePolicy1);
        line_3 = new QFrame(centralWidget);
        line_3->setObjectName(QStringLiteral("line_3"));
        line_3->setGeometry(QRect(10, 501, 541, 16));
        line_3->setFrameShape(QFrame::HLine);
        line_3->setFrameShadow(QFrame::Sunken);
        line_4 = new QFrame(centralWidget);
        line_4->setObjectName(QStringLiteral("line_4"));
        line_4->setGeometry(QRect(550, 406, 20, 211));
        line_4->setFrameShape(QFrame::VLine);
        line_4->setFrameShadow(QFrame::Sunken);
        cameraWidget_history = new CQtOpenCVViewerGl(centralWidget);
        cameraWidget_history->setObjectName(QStringLiteral("cameraWidget_history"));
        cameraWidget_history->setGeometry(QRect(1002, 22, 200, 150));
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(cameraWidget_history->sizePolicy().hasHeightForWidth());
        cameraWidget_history->setSizePolicy(sizePolicy2);
        label_history = new QLabel(centralWidget);
        label_history->setObjectName(QStringLiteral("label_history"));
        label_history->setGeometry(QRect(1022, 175, 181, 21));
        label_history->setAlignment(Qt::AlignCenter);
        verticalSlider_history = new QSlider(centralWidget);
        verticalSlider_history->setObjectName(QStringLiteral("verticalSlider_history"));
        verticalSlider_history->setGeometry(QRect(1212, 20, 19, 151));
        verticalSlider_history->setMinimum(-10);
        verticalSlider_history->setMaximum(0);
        verticalSlider_history->setValue(0);
        verticalSlider_history->setOrientation(Qt::Vertical);
        verticalSlider_history->setInvertedAppearance(false);
        verticalSlider_history->setTickPosition(QSlider::TicksBelow);
        verticalSlider_history->setTickInterval(1);
        pushButton_applyMorphologyEx = new QPushButton(centralWidget);
        pushButton_applyMorphologyEx->setObjectName(QStringLiteral("pushButton_applyMorphologyEx"));
        pushButton_applyMorphologyEx->setGeometry(QRect(10, 510, 91, 51));
        sizePolicy1.setHeightForWidth(pushButton_applyMorphologyEx->sizePolicy().hasHeightForWidth());
        pushButton_applyMorphologyEx->setSizePolicy(sizePolicy1);
        line_5 = new QFrame(centralWidget);
        line_5->setObjectName(QStringLiteral("line_5"));
        line_5->setGeometry(QRect(10, 556, 541, 16));
        line_5->setFrameShape(QFrame::HLine);
        line_5->setFrameShadow(QFrame::Sunken);
        label_morphologyExStructElemShape = new QLabel(centralWidget);
        label_morphologyExStructElemShape->setObjectName(QStringLiteral("label_morphologyExStructElemShape"));
        label_morphologyExStructElemShape->setGeometry(QRect(110, 510, 111, 16));
        label_morphologyExStructElemShape->setAlignment(Qt::AlignCenter);
        comboBox_morphologyExStructElemShape = new QComboBox(centralWidget);
        comboBox_morphologyExStructElemShape->setObjectName(QStringLiteral("comboBox_morphologyExStructElemShape"));
        comboBox_morphologyExStructElemShape->setGeometry(QRect(110, 530, 111, 22));
        label_morphologyExIterationNumber = new QLabel(centralWidget);
        label_morphologyExIterationNumber->setObjectName(QStringLiteral("label_morphologyExIterationNumber"));
        label_morphologyExIterationNumber->setGeometry(QRect(460, 508, 81, 20));
        lineEdit_morphologyExIterationNumber = new QLineEdit(centralWidget);
        lineEdit_morphologyExIterationNumber->setObjectName(QStringLiteral("lineEdit_morphologyExIterationNumber"));
        lineEdit_morphologyExIterationNumber->setGeometry(QRect(470, 530, 61, 21));
        lineEdit_morphologyExIterationNumber->setMaxLength(3);
        lineEdit_morphologyExIterationNumber->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        comboBox_morphologyExType = new QComboBox(centralWidget);
        comboBox_morphologyExType->setObjectName(QStringLiteral("comboBox_morphologyExType"));
        comboBox_morphologyExType->setGeometry(QRect(340, 529, 111, 22));
        label_morphologyExType_2 = new QLabel(centralWidget);
        label_morphologyExType_2->setObjectName(QStringLiteral("label_morphologyExType_2"));
        label_morphologyExType_2->setGeometry(QRect(340, 509, 111, 16));
        label_morphologyExType_2->setAlignment(Qt::AlignCenter);
        lineEdit_morphologyExStructElemSize = new QLineEdit(centralWidget);
        lineEdit_morphologyExStructElemSize->setObjectName(QStringLiteral("lineEdit_morphologyExStructElemSize"));
        lineEdit_morphologyExStructElemSize->setGeometry(QRect(250, 529, 61, 21));
        lineEdit_morphologyExStructElemSize->setMaxLength(3);
        lineEdit_morphologyExStructElemSize->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        label_morphologyExStructElemSize = new QLabel(centralWidget);
        label_morphologyExStructElemSize->setObjectName(QStringLiteral("label_morphologyExStructElemSize"));
        label_morphologyExStructElemSize->setGeometry(QRect(233, 507, 101, 20));
        pushButton_applyDilate = new QPushButton(centralWidget);
        pushButton_applyDilate->setObjectName(QStringLiteral("pushButton_applyDilate"));
        pushButton_applyDilate->setGeometry(QRect(10, 567, 91, 51));
        sizePolicy1.setHeightForWidth(pushButton_applyDilate->sizePolicy().hasHeightForWidth());
        pushButton_applyDilate->setSizePolicy(sizePolicy1);
        label_dilateIterationNumber = new QLabel(centralWidget);
        label_dilateIterationNumber->setObjectName(QStringLiteral("label_dilateIterationNumber"));
        label_dilateIterationNumber->setGeometry(QRect(460, 567, 81, 20));
        lineEdit_dilateIterationNumber = new QLineEdit(centralWidget);
        lineEdit_dilateIterationNumber->setObjectName(QStringLiteral("lineEdit_dilateIterationNumber"));
        lineEdit_dilateIterationNumber->setGeometry(QRect(470, 588, 61, 21));
        lineEdit_dilateIterationNumber->setMaxLength(3);
        lineEdit_dilateIterationNumber->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        label_dilateStructElemSize = new QLabel(centralWidget);
        label_dilateStructElemSize->setObjectName(QStringLiteral("label_dilateStructElemSize"));
        label_dilateStructElemSize->setGeometry(QRect(233, 564, 101, 20));
        lineEdit_dilateStructElemSize = new QLineEdit(centralWidget);
        lineEdit_dilateStructElemSize->setObjectName(QStringLiteral("lineEdit_dilateStructElemSize"));
        lineEdit_dilateStructElemSize->setGeometry(QRect(250, 586, 61, 21));
        lineEdit_dilateStructElemSize->setMaxLength(3);
        lineEdit_dilateStructElemSize->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        label_dilateStructElemShape = new QLabel(centralWidget);
        label_dilateStructElemShape->setObjectName(QStringLiteral("label_dilateStructElemShape"));
        label_dilateStructElemShape->setGeometry(QRect(110, 567, 111, 16));
        label_dilateStructElemShape->setAlignment(Qt::AlignCenter);
        comboBox_dilateStructElemShape = new QComboBox(centralWidget);
        comboBox_dilateStructElemShape->setObjectName(QStringLiteral("comboBox_dilateStructElemShape"));
        comboBox_dilateStructElemShape->setGeometry(QRect(110, 587, 111, 22));
        checkBox_thresholdOtsu = new QCheckBox(centralWidget);
        checkBox_thresholdOtsu->setObjectName(QStringLiteral("checkBox_thresholdOtsu"));
        checkBox_thresholdOtsu->setGeometry(QRect(420, 429, 91, 17));
        checkBox_thresholdOtsu->setChecked(true);
        label_colorConversionValue = new QLabel(centralWidget);
        label_colorConversionValue->setObjectName(QStringLiteral("label_colorConversionValue"));
        label_colorConversionValue->setGeometry(QRect(466, 456, 81, 20));
        lineEdit_colorConversionValue = new QLineEdit(centralWidget);
        lineEdit_colorConversionValue->setObjectName(QStringLiteral("lineEdit_colorConversionValue"));
        lineEdit_colorConversionValue->setGeometry(QRect(470, 478, 61, 21));
        lineEdit_colorConversionValue->setMaxLength(3);
        lineEdit_colorConversionValue->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        pushButton_applyDistanceTransform = new QPushButton(centralWidget);
        pushButton_applyDistanceTransform->setObjectName(QStringLiteral("pushButton_applyDistanceTransform"));
        pushButton_applyDistanceTransform->setGeometry(QRect(570, 401, 91, 51));
        sizePolicy1.setHeightForWidth(pushButton_applyDistanceTransform->sizePolicy().hasHeightForWidth());
        pushButton_applyDistanceTransform->setSizePolicy(sizePolicy1);
        comboBox_distanceTransformType = new QComboBox(centralWidget);
        comboBox_distanceTransformType->setObjectName(QStringLiteral("comboBox_distanceTransformType"));
        comboBox_distanceTransformType->setGeometry(QRect(670, 421, 111, 22));
        label_distanceTransformType = new QLabel(centralWidget);
        label_distanceTransformType->setObjectName(QStringLiteral("label_distanceTransformType"));
        label_distanceTransformType->setGeometry(QRect(670, 401, 111, 16));
        label_distanceTransformType->setAlignment(Qt::AlignCenter);
        comboBox_distanceTransformMaskSize = new QComboBox(centralWidget);
        comboBox_distanceTransformMaskSize->setObjectName(QStringLiteral("comboBox_distanceTransformMaskSize"));
        comboBox_distanceTransformMaskSize->setGeometry(QRect(790, 421, 151, 22));
        label_distanceTransformMaskSize = new QLabel(centralWidget);
        label_distanceTransformMaskSize->setObjectName(QStringLiteral("label_distanceTransformMaskSize"));
        label_distanceTransformMaskSize->setGeometry(QRect(810, 401, 111, 16));
        label_distanceTransformMaskSize->setAlignment(Qt::AlignCenter);
        comboBox_distanceTransformLabelType = new QComboBox(centralWidget);
        comboBox_distanceTransformLabelType->setObjectName(QStringLiteral("comboBox_distanceTransformLabelType"));
        comboBox_distanceTransformLabelType->setGeometry(QRect(950, 421, 141, 22));
        label_distanceTransformLabelType = new QLabel(centralWidget);
        label_distanceTransformLabelType->setObjectName(QStringLiteral("label_distanceTransformLabelType"));
        label_distanceTransformLabelType->setGeometry(QRect(950, 401, 111, 16));
        label_distanceTransformLabelType->setAlignment(Qt::AlignCenter);
        pushButton_applySubtract = new QPushButton(centralWidget);
        pushButton_applySubtract->setObjectName(QStringLiteral("pushButton_applySubtract"));
        pushButton_applySubtract->setGeometry(QRect(571, 455, 91, 51));
        sizePolicy1.setHeightForWidth(pushButton_applySubtract->sizePolicy().hasHeightForWidth());
        pushButton_applySubtract->setSizePolicy(sizePolicy1);
        pushButton_applyConnectedComponents = new QPushButton(centralWidget);
        pushButton_applyConnectedComponents->setObjectName(QStringLiteral("pushButton_applyConnectedComponents"));
        pushButton_applyConnectedComponents->setGeometry(QRect(570, 510, 91, 51));
        sizePolicy1.setHeightForWidth(pushButton_applyConnectedComponents->sizePolicy().hasHeightForWidth());
        pushButton_applyConnectedComponents->setSizePolicy(sizePolicy1);
        line_9 = new QFrame(centralWidget);
        line_9->setObjectName(QStringLiteral("line_9"));
        line_9->setGeometry(QRect(566, 446, 541, 16));
        line_9->setFrameShape(QFrame::HLine);
        line_9->setFrameShadow(QFrame::Sunken);
        line_11 = new QFrame(centralWidget);
        line_11->setObjectName(QStringLiteral("line_11"));
        line_11->setGeometry(QRect(566, 556, 541, 16));
        line_11->setFrameShape(QFrame::HLine);
        line_11->setFrameShadow(QFrame::Sunken);
        line_13 = new QFrame(centralWidget);
        line_13->setObjectName(QStringLiteral("line_13"));
        line_13->setGeometry(QRect(566, 501, 541, 16));
        line_13->setFrameShape(QFrame::HLine);
        line_13->setFrameShadow(QFrame::Sunken);
        frame_processedImage = new QFrame(centralWidget);
        frame_processedImage->setObjectName(QStringLiteral("frame_processedImage"));
        frame_processedImage->setGeometry(QRect(508, 3, 484, 364));
        frame_processedImage->setStyleSheet(QStringLiteral(""));
        frame_processedImage->setFrameShape(QFrame::StyledPanel);
        frame_processedImage->setFrameShadow(QFrame::Raised);
        cameraWidget_right = new CQtOpenCVViewerGl(frame_processedImage);
        cameraWidget_right->setObjectName(QStringLiteral("cameraWidget_right"));
        cameraWidget_right->setGeometry(QRect(2, 2, 480, 360));
        sizePolicy2.setHeightForWidth(cameraWidget_right->sizePolicy().hasHeightForWidth());
        cameraWidget_right->setSizePolicy(sizePolicy2);
        frame = new QFrame(centralWidget);
        frame->setObjectName(QStringLiteral("frame"));
        frame->setGeometry(QRect(14, 3, 484, 364));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        cameraWidget_left = new CQtOpenCVViewerGl(frame);
        cameraWidget_left->setObjectName(QStringLiteral("cameraWidget_left"));
        cameraWidget_left->setGeometry(QRect(2, 2, 480, 360));
        sizePolicy2.setHeightForWidth(cameraWidget_left->sizePolicy().hasHeightForWidth());
        cameraWidget_left->setSizePolicy(sizePolicy2);
        comboBox_subtractSrc1 = new QComboBox(centralWidget);
        comboBox_subtractSrc1->setObjectName(QStringLiteral("comboBox_subtractSrc1"));
        comboBox_subtractSrc1->setGeometry(QRect(671, 458, 111, 22));
        comboBox_subtractSrc2 = new QComboBox(centralWidget);
        comboBox_subtractSrc2->setObjectName(QStringLiteral("comboBox_subtractSrc2"));
        comboBox_subtractSrc2->setGeometry(QRect(821, 458, 111, 22));
        label_subtractMinus = new QLabel(centralWidget);
        label_subtractMinus->setObjectName(QStringLiteral("label_subtractMinus"));
        label_subtractMinus->setGeometry(QRect(786, 458, 31, 20));
        QFont font;
        font.setPointSize(18);
        label_subtractMinus->setFont(font);
        label_subtractMinus->setAlignment(Qt::AlignCenter);
        pushButton_applyWatershed = new QPushButton(centralWidget);
        pushButton_applyWatershed->setObjectName(QStringLiteral("pushButton_applyWatershed"));
        pushButton_applyWatershed->setGeometry(QRect(570, 566, 91, 51));
        sizePolicy1.setHeightForWidth(pushButton_applyWatershed->sizePolicy().hasHeightForWidth());
        pushButton_applyWatershed->setSizePolicy(sizePolicy1);
        comboBox_subtractResult = new QComboBox(centralWidget);
        comboBox_subtractResult->setObjectName(QStringLiteral("comboBox_subtractResult"));
        comboBox_subtractResult->setEnabled(false);
        comboBox_subtractResult->setGeometry(QRect(980, 458, 111, 22));
        label_subtractEqual = new QLabel(centralWidget);
        label_subtractEqual->setObjectName(QStringLiteral("label_subtractEqual"));
        label_subtractEqual->setGeometry(QRect(939, 457, 31, 20));
        label_subtractEqual->setFont(font);
        label_subtractEqual->setAlignment(Qt::AlignCenter);
        lineEdit_subtractSrc1 = new QLineEdit(centralWidget);
        lineEdit_subtractSrc1->setObjectName(QStringLiteral("lineEdit_subtractSrc1"));
        lineEdit_subtractSrc1->setGeometry(QRect(670, 482, 113, 20));
        lineEdit_subtractSrc1->setMaxLength(3);
        lineEdit_subtractSrc2 = new QLineEdit(centralWidget);
        lineEdit_subtractSrc2->setObjectName(QStringLiteral("lineEdit_subtractSrc2"));
        lineEdit_subtractSrc2->setGeometry(QRect(820, 482, 113, 20));
        lineEdit_subtractSrc2->setMaxLength(3);
        comboBox_connectedComponentsConnectivity = new QComboBox(centralWidget);
        comboBox_connectedComponentsConnectivity->setObjectName(QStringLiteral("comboBox_connectedComponentsConnectivity"));
        comboBox_connectedComponentsConnectivity->setGeometry(QRect(670, 524, 111, 22));
        comboBox_connectedComponentsLType = new QComboBox(centralWidget);
        comboBox_connectedComponentsLType->setObjectName(QStringLiteral("comboBox_connectedComponentsLType"));
        comboBox_connectedComponentsLType->setGeometry(QRect(820, 524, 111, 22));
        textEdit_detailedError = new QTextEdit(centralWidget);
        textEdit_detailedError->setObjectName(QStringLiteral("textEdit_detailedError"));
        textEdit_detailedError->setGeometry(QRect(1000, 260, 231, 131));
        pushButton_getDetailedError = new QPushButton(centralWidget);
        pushButton_getDetailedError->setObjectName(QStringLiteral("pushButton_getDetailedError"));
        pushButton_getDetailedError->setGeometry(QRect(1070, 210, 81, 41));
        sizePolicy1.setHeightForWidth(pushButton_getDetailedError->sizePolicy().hasHeightForWidth());
        pushButton_getDetailedError->setSizePolicy(sizePolicy1);
        comboBox_applyWatershedMarkerImage = new QComboBox(centralWidget);
        comboBox_applyWatershedMarkerImage->setObjectName(QStringLiteral("comboBox_applyWatershedMarkerImage"));
        comboBox_applyWatershedMarkerImage->setGeometry(QRect(670, 580, 111, 22));
        lineEdit_colorConversionNumChannels = new QLineEdit(centralWidget);
        lineEdit_colorConversionNumChannels->setObjectName(QStringLiteral("lineEdit_colorConversionNumChannels"));
        lineEdit_colorConversionNumChannels->setGeometry(QRect(330, 478, 61, 21));
        lineEdit_colorConversionNumChannels->setMaxLength(1);
        lineEdit_colorConversionNumChannels->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        label_colorConversionNumChannels = new QLabel(centralWidget);
        label_colorConversionNumChannels->setObjectName(QStringLiteral("label_colorConversionNumChannels"));
        label_colorConversionNumChannels->setGeometry(QRect(316, 457, 101, 20));
        label_colorConversionDepth = new QLabel(centralWidget);
        label_colorConversionDepth->setObjectName(QStringLiteral("label_colorConversionDepth"));
        label_colorConversionDepth->setGeometry(QRect(160, 457, 91, 20));
        comboBox_colorConversionDepth = new QComboBox(centralWidget);
        comboBox_colorConversionDepth->setObjectName(QStringLiteral("comboBox_colorConversionDepth"));
        comboBox_colorConversionDepth->setGeometry(QRect(110, 477, 191, 22));
        line_6 = new QFrame(centralWidget);
        line_6->setObjectName(QStringLiteral("line_6"));
        line_6->setGeometry(QRect(10, 612, 1231, 16));
        line_6->setFrameShape(QFrame::HLine);
        line_6->setFrameShadow(QFrame::Sunken);
        line_7 = new QFrame(centralWidget);
        line_7->setObjectName(QStringLiteral("line_7"));
        line_7->setGeometry(QRect(1103, 405, 20, 211));
        line_7->setFrameShape(QFrame::VLine);
        line_7->setFrameShadow(QFrame::Sunken);
        CMainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(CMainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1250, 21));
        menuCamera = new QMenu(menuBar);
        menuCamera->setObjectName(QStringLiteral("menuCamera"));
        menuImage = new QMenu(menuBar);
        menuImage->setObjectName(QStringLiteral("menuImage"));
        menuDocumentation = new QMenu(menuBar);
        menuDocumentation->setObjectName(QStringLiteral("menuDocumentation"));
        menuOpenCv_3_Functions = new QMenu(menuDocumentation);
        menuOpenCv_3_Functions->setObjectName(QStringLiteral("menuOpenCv_3_Functions"));
        menuImage_Processing = new QMenu(menuOpenCv_3_Functions);
        menuImage_Processing->setObjectName(QStringLiteral("menuImage_Processing"));
        menuScript = new QMenu(menuBar);
        menuScript->setObjectName(QStringLiteral("menuScript"));
        CMainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(CMainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        mainToolBar->setMovable(false);
        CMainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(CMainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        CMainWindow->setStatusBar(statusBar);

        menuBar->addAction(menuCamera->menuAction());
        menuBar->addAction(menuImage->menuAction());
        menuBar->addAction(menuScript->menuAction());
        menuBar->addAction(menuDocumentation->menuAction());
        menuCamera->addAction(actionStart);
        menuCamera->addAction(actionStop);
        menuImage->addAction(actionLoad_Image);
        menuImage->addAction(actionSave_processed_image);
        menuImage->addAction(actionReset);
        menuDocumentation->addAction(actionOpen_Howto);
        menuDocumentation->addAction(menuOpenCv_3_Functions->menuAction());
        menuOpenCv_3_Functions->addAction(menuImage_Processing->menuAction());
        menuImage_Processing->addAction(actionWatersheed);
        menuScript->addAction(actionLoad_and_Execute_script);

        retranslateUi(CMainWindow);

        comboBox_distanceTransformType->setCurrentIndex(0);
        comboBox_distanceTransformMaskSize->setCurrentIndex(0);
        comboBox_distanceTransformLabelType->setCurrentIndex(2);
        comboBox_subtractSrc1->setCurrentIndex(0);
        comboBox_subtractSrc2->setCurrentIndex(1);
        comboBox_subtractResult->setCurrentIndex(1);
        comboBox_applyWatershedMarkerImage->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(CMainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *CMainWindow)
    {
        CMainWindow->setWindowTitle(QApplication::translate("CMainWindow", "CMainWindow", 0));
        actionStart->setText(QApplication::translate("CMainWindow", "Start", 0));
        actionStop->setText(QApplication::translate("CMainWindow", "Stop", 0));
        actionReset->setText(QApplication::translate("CMainWindow", "Reset image to original", 0));
        actionLoad_Image->setText(QApplication::translate("CMainWindow", "Load Image", 0));
        actionOpen_Howto->setText(QApplication::translate("CMainWindow", "Open Howto", 0));
        actionSave_processed_image->setText(QApplication::translate("CMainWindow", "Save selected image", 0));
        actionLoad_and_Execute_script->setText(QApplication::translate("CMainWindow", "Load and Execute script", 0));
        actionWatersheed->setText(QApplication::translate("CMainWindow", "Watersheed", 0));
        groupBox->setTitle(QApplication::translate("CMainWindow", "Options", 0));
        checkBox_fullFace->setText(QApplication::translate("CMainWindow", "Detect Full Face", 0));
        checkBox_eyes->setText(QApplication::translate("CMainWindow", "Detect Eyes", 0));
        checkBox_nose->setText(QApplication::translate("CMainWindow", "Detect Nose", 0));
        checkBox_mouth->setText(QApplication::translate("CMainWindow", "Detect Mouth", 0));
        pushButton_applyThreshold->setText(QApplication::translate("CMainWindow", "Apply Threshold", 0));
        comboBox_thresholdType->clear();
        comboBox_thresholdType->insertItems(0, QStringList()
         << QApplication::translate("CMainWindow", "BINARY", 0)
         << QApplication::translate("CMainWindow", "BINARY_INV", 0)
         << QApplication::translate("CMainWindow", "TRUNC", 0)
         << QApplication::translate("CMainWindow", "TOZERO", 0)
         << QApplication::translate("CMainWindow", "TOZERO_INV", 0)
        );
        label_thresholdType->setText(QApplication::translate("CMainWindow", "Threshold type", 0));
        label_thresholdValue->setText(QApplication::translate("CMainWindow", "Threshold value", 0));
        lineEdit_thresholdValue->setText(QApplication::translate("CMainWindow", "127", 0));
        label_thresholdMax->setText(QApplication::translate("CMainWindow", "Max value", 0));
        lineEdit_thresholdMax->setText(QApplication::translate("CMainWindow", "255", 0));
        label_rightImage->setText(QApplication::translate("CMainWindow", "Right image", 0));
        label_leftImage->setText(QApplication::translate("CMainWindow", "Left image", 0));
        pushButton_applyGreyScale->setText(QApplication::translate("CMainWindow", "Apply Color\n"
"Conversion", 0));
        label_history->setText(QApplication::translate("CMainWindow", "History", 0));
        pushButton_applyMorphologyEx->setText(QApplication::translate("CMainWindow", "Apply \n"
"MorphologyEx", 0));
        label_morphologyExStructElemShape->setText(QApplication::translate("CMainWindow", "Struct element shape", 0));
        comboBox_morphologyExStructElemShape->clear();
        comboBox_morphologyExStructElemShape->insertItems(0, QStringList()
         << QApplication::translate("CMainWindow", "MORPH_RECT", 0)
         << QApplication::translate("CMainWindow", "MORPH_ELLIPSE", 0)
         << QApplication::translate("CMainWindow", "MORPH_CROSS", 0)
        );
        label_morphologyExIterationNumber->setText(QApplication::translate("CMainWindow", "Iteration number", 0));
        lineEdit_morphologyExIterationNumber->setText(QApplication::translate("CMainWindow", "2", 0));
        comboBox_morphologyExType->clear();
        comboBox_morphologyExType->insertItems(0, QStringList()
         << QApplication::translate("CMainWindow", "MORPH_OPEN", 0)
         << QApplication::translate("CMainWindow", "MORPH_CLOSE", 0)
         << QApplication::translate("CMainWindow", "MORPH_GRADIENT ", 0)
         << QApplication::translate("CMainWindow", "MORPH_TOPHAT", 0)
         << QApplication::translate("CMainWindow", "MORPH_BLACKHAT ", 0)
        );
        label_morphologyExType_2->setText(QApplication::translate("CMainWindow", "Morphology type", 0));
        lineEdit_morphologyExStructElemSize->setText(QApplication::translate("CMainWindow", "3", 0));
        label_morphologyExStructElemSize->setText(QApplication::translate("CMainWindow", "Struct element size", 0));
        pushButton_applyDilate->setText(QApplication::translate("CMainWindow", "Apply Dilate", 0));
        label_dilateIterationNumber->setText(QApplication::translate("CMainWindow", "Iteration number", 0));
        lineEdit_dilateIterationNumber->setText(QApplication::translate("CMainWindow", "3", 0));
        label_dilateStructElemSize->setText(QApplication::translate("CMainWindow", "Struct element size", 0));
        lineEdit_dilateStructElemSize->setText(QApplication::translate("CMainWindow", "3", 0));
        label_dilateStructElemShape->setText(QApplication::translate("CMainWindow", "Struct element shape", 0));
        comboBox_dilateStructElemShape->clear();
        comboBox_dilateStructElemShape->insertItems(0, QStringList()
         << QApplication::translate("CMainWindow", "MORPH_RECT", 0)
         << QApplication::translate("CMainWindow", "MORPH_ELLIPSE", 0)
         << QApplication::translate("CMainWindow", "MORPH_CROSS", 0)
        );
        checkBox_thresholdOtsu->setText(QApplication::translate("CMainWindow", "Apply OTSU", 0));
        label_colorConversionValue->setText(QApplication::translate("CMainWindow", "Conversion code", 0));
#ifndef QT_NO_TOOLTIP
        lineEdit_colorConversionValue->setToolTip(QApplication::translate("CMainWindow", "<html><head/><body><p>The color space goes from 0 to 135.</p><p>Relevant values are:</p><p>- 7 = <span style=\" color:#800080;\">CV_RGB2GRAY</span></p><p><br/></p></body></html>", 0));
#endif // QT_NO_TOOLTIP
        lineEdit_colorConversionValue->setText(QApplication::translate("CMainWindow", "7", 0));
        pushButton_applyDistanceTransform->setText(QApplication::translate("CMainWindow", "Apply\n"
"Distance Transf.", 0));
        comboBox_distanceTransformType->clear();
        comboBox_distanceTransformType->insertItems(0, QStringList()
         << QApplication::translate("CMainWindow", "CV_DIST_L1", 0)
         << QApplication::translate("CMainWindow", "CV_DIST_L2", 0)
         << QApplication::translate("CMainWindow", "CV_DIST_C", 0)
        );
        label_distanceTransformType->setText(QApplication::translate("CMainWindow", "Type", 0));
        comboBox_distanceTransformMaskSize->clear();
        comboBox_distanceTransformMaskSize->insertItems(0, QStringList()
         << QApplication::translate("CMainWindow", "3 x 3", 0)
         << QApplication::translate("CMainWindow", "5 x 5", 0)
         << QApplication::translate("CMainWindow", "CV_DIST_MASK_PRECISE ", 0)
        );
        label_distanceTransformMaskSize->setText(QApplication::translate("CMainWindow", "Mask size", 0));
        comboBox_distanceTransformLabelType->clear();
        comboBox_distanceTransformLabelType->insertItems(0, QStringList()
         << QApplication::translate("CMainWindow", "DIST_LABEL_CCOMP", 0)
         << QApplication::translate("CMainWindow", "DIST_LABEL_PIXEL", 0)
         << QApplication::translate("CMainWindow", "NOT_USED", 0)
        );
        label_distanceTransformLabelType->setText(QApplication::translate("CMainWindow", "Label type", 0));
        pushButton_applySubtract->setText(QApplication::translate("CMainWindow", "Apply Subtract", 0));
        pushButton_applyConnectedComponents->setText(QApplication::translate("CMainWindow", "Apply\n"
"Connected\n"
"Components", 0));
        comboBox_subtractSrc1->clear();
        comboBox_subtractSrc1->insertItems(0, QStringList()
         << QApplication::translate("CMainWindow", "LEFT IMAGE", 0)
         << QApplication::translate("CMainWindow", "RIGHT IMAGE", 0)
         << QApplication::translate("CMainWindow", "USE SCALAR", 0)
        );
        comboBox_subtractSrc2->clear();
        comboBox_subtractSrc2->insertItems(0, QStringList()
         << QApplication::translate("CMainWindow", "LEFT IMAGE", 0)
         << QApplication::translate("CMainWindow", "RIGHT IMAGE", 0)
         << QApplication::translate("CMainWindow", "USE SCALAR", 0)
        );
        label_subtractMinus->setText(QApplication::translate("CMainWindow", "-", 0));
        pushButton_applyWatershed->setText(QApplication::translate("CMainWindow", "Apply\n"
"Watershed", 0));
        comboBox_subtractResult->clear();
        comboBox_subtractResult->insertItems(0, QStringList()
         << QApplication::translate("CMainWindow", "LEFT IMAGE", 0)
         << QApplication::translate("CMainWindow", "RIGHT IMAGE", 0)
        );
        label_subtractEqual->setText(QApplication::translate("CMainWindow", "=", 0));
        lineEdit_subtractSrc1->setText(QApplication::translate("CMainWindow", "0", 0));
        lineEdit_subtractSrc2->setText(QApplication::translate("CMainWindow", "0", 0));
        comboBox_connectedComponentsConnectivity->clear();
        comboBox_connectedComponentsConnectivity->insertItems(0, QStringList()
         << QApplication::translate("CMainWindow", "4-WAY", 0)
         << QApplication::translate("CMainWindow", "8-WAY", 0)
        );
        comboBox_connectedComponentsLType->clear();
        comboBox_connectedComponentsLType->insertItems(0, QStringList()
         << QApplication::translate("CMainWindow", "CV_32S", 0)
         << QApplication::translate("CMainWindow", "CV_16U", 0)
        );
        pushButton_getDetailedError->setText(QApplication::translate("CMainWindow", "Get\n"
"Detailed Error", 0));
        comboBox_applyWatershedMarkerImage->clear();
        comboBox_applyWatershedMarkerImage->insertItems(0, QStringList()
         << QApplication::translate("CMainWindow", "LEFT IMAGE", 0)
         << QApplication::translate("CMainWindow", "RIGHT IMAGE", 0)
         << QApplication::translate("CMainWindow", "USE SCALAR", 0)
        );
#ifndef QT_NO_TOOLTIP
        lineEdit_colorConversionNumChannels->setToolTip(QApplication::translate("CMainWindow", "<html><head/><body><p>Number of channels in the destination image. </p><p>If this number is 0 the result is calculated form the source image and the conversion code.</p><p><br/></p></body></html>", 0));
#endif // QT_NO_TOOLTIP
        lineEdit_colorConversionNumChannels->setText(QApplication::translate("CMainWindow", "0", 0));
        label_colorConversionNumChannels->setText(QApplication::translate("CMainWindow", "Number of channels", 0));
        label_colorConversionDepth->setText(QApplication::translate("CMainWindow", "Conversion mode", 0));
        menuCamera->setTitle(QApplication::translate("CMainWindow", "Camera", 0));
        menuImage->setTitle(QApplication::translate("CMainWindow", "Image", 0));
        menuDocumentation->setTitle(QApplication::translate("CMainWindow", "Documentation", 0));
        menuOpenCv_3_Functions->setTitle(QApplication::translate("CMainWindow", "OpenCv 3 Functions", 0));
        menuImage_Processing->setTitle(QApplication::translate("CMainWindow", "Image Processing", 0));
        menuScript->setTitle(QApplication::translate("CMainWindow", "Script", 0));
    } // retranslateUi

};

namespace Ui {
    class CMainWindow: public Ui_CMainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CMAINWINDOW_H
