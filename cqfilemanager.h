﻿#ifndef CQFILEMANAGER_H
#define CQFILEMANAGER_H

#include <QObject>
#include <QFileDialog>

class CQfilemanager : public QObject
{
    Q_OBJECT
public:
    explicit CQfilemanager(QObject *parent = 0);
    ~CQfilemanager();

    QFile::FileError Open(QString &filenamebool, QFile::OpenMode mode, bool must_exist);
    void Close(void);
    bool IsOpen(void);
    quint32 GetCurrentLineLength(void);
    quint32 GetCurrentPos(void);
    quint32 GetCurrentLine(void);
    QFile::FileError Seek(quint32 position);
    bool Resize(quint32 size);
    QFile::FileError SeekLine(quint32 line);
    QFile::FileError WriteLine(const char* data);
    QFile::FileError WriteRaw(const char* data, quint32 size);
    QFile::FileError WriteLine(const char* data, quint32 size);
    QFile::FileError ReadRaw(char* data, quint32 size);
    QFile::FileError ReadLine(char* data, quint32 size);
    QFile::FileError ReadLine(QString& tmpStr);
    QFile::FileError SaveSetting(const QString& parameter_name, const QString& value);
    QFile::FileError SaveSetting(const QString& parameter_name, const int& value);
    QFile::FileError LoadSetting(const QString& parameter_name, QString& value);
    QFile::FileError LoadSetting(const QString& parameter_name, int& value);
    QFile::FileError LoadSetting(const QString& parameter_name, quint32& value,bool HexFlag);
    QFile::FileError LoadSetting(const QString &parameter_name, quint32& value);
    QStringList OpenFileDialog(QWidget *parent, QFileDialog::FileMode file_mode, QString &directory, const QString &window_title, const QString &filename_filter);

private:
    QFile *fp;
    quint32 currentLineLength;
    quint32 currentLine;
};

#endif // CQFILEMANAGER_H
