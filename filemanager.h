/****************************************************************************
**
** Copyright (C) 2011 STMicroelectronics. All rights reserved.
**
** This file is part of MDR3 Control Application
**
** Author : Luigi Cotignano (luigi.cotignano@st.com)
**
** This file may be used under the terms of the GNU General Public License
** version 2.0 as published by the Free Software Foundation or under the
** terms of the Qt Commercial License Agreement. The respective license
** texts for these are provided with the open source and commercial
** editions of Qt.
**
****************************************************************************/

#ifndef FILEMANAGER_H
#define FILEMANAGER_H

#include <QObject>
#include <QFileDialog>
#include "common.h"

class FileManager : public QObject
{
    Q_OBJECT

public:
    explicit FileManager(QObject *parent = 0);
    ~FileManager();

    QFile::FileError    open(QString &filenamebool, QFile::OpenMode mode, bool must_exist);
    void                close(void);
    bool                isOpen(void);
    quint32             getCurrentLineLength(void);
    quint32             getCurrentPos(void);
    quint32             getCurrentLine(void);
    QFile::FileError    seek(quint32 position);
    bool                resize(quint32 size);
    QFile::FileError    seekLine(quint32 line);
    QFile::FileError    writeLine(const char* data);
    QFile::FileError    writeRaw(const char* data, quint32 size);
    QFile::FileError    writeLine(const char* data, quint32 size);
    QFile::FileError    readRaw(char* data, quint32 size);
    QFile::FileError    readLine(char* data, quint32 size);
    QFile::FileError    readLine(QString& data);
    QFile::FileError    saveSetting(const QString& parameter_name, const QString& value);
    QFile::FileError    saveSetting(const QString& parameter_name, const int& value);
    QFile::FileError    loadSetting(const QString& parameter_name, QString& value);
    QFile::FileError    loadSetting(const QString& parameter_name, int& value);
    QFile::FileError    loadSetting(const QString& parameter_name, quint32& value,bool HexFlag);
    QFile::FileError    loadSetting(const QString &parameter_name, quint32& value);
    QStringList         openFileDialog(QWidget *parent, QFileDialog::FileMode file_mode, QString &directory, const QString &window_title, const QString &filename_filter);

private:
    QFile               *fp;
    quint32             current_line_length;
    quint32             current_line;
};

#endif // FILEMANAGER_H
