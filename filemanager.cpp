/****************************************************************************
**
** Copyright (C) 2011 STMicroelectronics. All rights reserved.
**
** This file is part of MDR3 Control Application
**
** Author : Luigi Cotignano (luigi.cotignano@st.com)
**
** This file may be used under the terms of the GNU General Public License
** version 2.0 as published by the Free Software Foundation or under the
** terms of the Qt Commercial License Agreement. The respective license
** texts for these are provided with the open source and commercial
** editions of Qt.
**
****************************************************************************/

#include "filemanager.h"

/***************************************************************************
* Function : FileManager
****************************************************************************/
FileManager::FileManager(QObject *parent): QObject(parent), fp(0), current_line_length(0)
{
//    qDebug() << "Created: FileManager";
}

/***************************************************************************
* Function : ~FileManager
****************************************************************************/
FileManager::~FileManager()
{
//    qDebug() << "Deleted: FileManager";
}

/***************************************************************************
* Function : resize
****************************************************************************/
 bool FileManager::resize(quint32 size)
{
    return fp->resize(size);
}

/***************************************************************************
* Function : open
****************************************************************************/
QFile::FileError FileManager::open(QString &filename, QFile::OpenMode mode, bool must_exist)
{
    QFile::FileError error = QFile::NoError;

    current_line = 0;

    bool file_exist = QFile().exists(filename);

    if (!file_exist && must_exist)
    {
        error = QFile::OpenError;
    }
    else
    {
        fp = new QFile(filename);
        if (!fp->open(mode))
            error = QFile::OpenError;
    }

    return error;
}

/***************************************************************************
* Function : close
****************************************************************************/
void FileManager::close(void)
{
    if (fp->handle() != -1)
        fp->close();

    fp = 0;
}

/***************************************************************************
* Function : seek
****************************************************************************/
QFile::FileError FileManager::seek(quint32 position)
{
    if (!fp->isOpen())
    {
        return QFile::OpenError;
    }

    if (position == 0)
        current_line = 0;

    fp->seek((int)position);
    return QFile::NoError;
}

/***************************************************************************
* Function : seekLine
****************************************************************************/
QFile::FileError FileManager::seekLine(quint32 line)
{
    QFile::FileError error = QFile::NoError;
    qint64 read_bytes = 0;
    quint32 i = 0;

    if (!fp->isOpen())
    {
        error = QFile::OpenError;
        return error;
    }

    fp->seek(0);

    char temp[255];

    while ((i++ < line) && (read_bytes >= 0))
    {
        read_bytes = fp->readLine(temp, MAX_FILE_LINE_LENGTH);
    }

    i--;

    if ((i == line) && (read_bytes >= 0))
    {
        current_line = line;
        error = QFile::NoError;
    }
    else
    {
        current_line = 0;
        error = QFile::ReadError;
    }
    return error;
}

/***************************************************************************
* Function : writeRaw
****************************************************************************/
QFile::FileError FileManager::writeRaw(const char* data, quint32 size)
{
    if (!fp->isOpen())
    {
        return QFile::OpenError;
    }

    fp->write(data, size);
    return QFile::NoError;
}

/***************************************************************************
* Function : writeLine
****************************************************************************/
QFile::FileError FileManager::writeLine(const char* data, quint32 size)
{
    if (!fp->isOpen())
    {
        return QFile::OpenError;
    }

    fp->write(data, size);
    return QFile::NoError;
}

/***************************************************************************
* Function : writeLine
****************************************************************************/
QFile::FileError FileManager::writeLine(const char* data)
{
    const char terminator = '\n';

    if (!fp->isOpen())
    {
        return QFile::OpenError;
    }

    fp->write(data, sizeof(data)+1);
    fp->write(&terminator,1);

    return QFile::NoError;
}

/***************************************************************************
* Function : readRaw
****************************************************************************/
QFile::FileError FileManager::readRaw(char* data, quint32 size)
{
    QFile::FileError return_code = QFile::NoError;

    if (!fp->isOpen())
    {
        return QFile::OpenError;
    }

    int ret = fp->read(data, size);

    // No error (0) or Only "n" bytes were read (>0)
    if (ret >= 0)
    {
        return_code = QFile::NoError;
    }
    // Nothing was read
    else if (ret == -1)
    {
        return_code = QFile::ReadError;
    }
    return return_code;
}

/***************************************************************************
* Function : readLine
****************************************************************************/
QFile::FileError FileManager::readLine(char* data, quint32 size)
{
    QFile::FileError return_code = QFile::NoError;

    if (!fp->isOpen())
    {
        current_line_length = -1;
        return QFile::OpenError;
    }

    int ret = fp->readLine(data, size);

    // No error (0) or Only "n" bytes were read (>0)
    if (ret >= 0)
    {
        return_code = QFile::NoError;
        current_line++;
    }
    // Nothing was read
    else if (ret == -1)
    {
        return_code = QFile::ReadError;
    }
    current_line_length = ret;

    return return_code;
}

/***************************************************************************
* Function : readLine (overloaded)
****************************************************************************/
QFile::FileError FileManager::readLine(QString &stringa)
{
    QFile::FileError return_code = QFile::NoError;
    char data[255] = {0};

    if (!fp->isOpen())
    {
        current_line_length = -1;
        return QFile::OpenError;
    }

    int ret = fp->readLine(data, sizeof(data));

    stringa = QString(data).trimmed();

    // Skip Commented lines
    if (stringa.startsWith("//"))
    {
        stringa = NULL_STRING;
        ret = 0;
    }

    // Nothing was read (EOF reached) //or NULL string
    if (ret == -1)// || stringa.isEmpty())
    {
        return_code = QFile::ReadError;
    }
    // No error (0) or Only "n" bytes were read (>0)
    else if (ret >= 0)
    {
        return_code = QFile::NoError;
        current_line++;
    }
    current_line_length = ret;

    return return_code;
}

/***************************************************************************
* Function : getCurrentLineLength
****************************************************************************/
quint32 FileManager::getCurrentLineLength(void)
{
    return current_line_length;
}

/***************************************************************************
* Function : isOpen
****************************************************************************/
bool FileManager::isOpen(void)
{
    if (fp)
        return fp->isOpen();
    else
        return false;
}

/***************************************************************************
* Function : getCurrentPos
****************************************************************************/
quint32 FileManager::getCurrentPos(void)
{
    return fp->pos();
}

/***************************************************************************
* Function : getCurrentLine
****************************************************************************/
quint32 FileManager::getCurrentLine(void)
{
    if (current_line > 0)
        return (current_line - 1);
    else
        return current_line;
}

/***************************************************************************
* Function : openFileDialog
****************************************************************************/
QStringList FileManager::openFileDialog(QWidget *parent, QFileDialog::FileMode file_mode, QString &directory, const QString &window_title, const QString &filename_filter)
{
    QFileDialog *openFileDialog = new QFileDialog(parent);
    QStringList fileNames;

    openFileDialog->setFileMode(file_mode);
    openFileDialog->setDirectory(directory);
    openFileDialog->setWindowTitle(window_title);
    openFileDialog->setNameFilter(filename_filter);
    openFileDialog->exec();

    directory = openFileDialog->directory().absolutePath();

    fileNames = openFileDialog->selectedFiles();

    return fileNames;
}

/***************************************************************************
* Function : saveSetting
****************************************************************************/
QFile::FileError FileManager::saveSetting(const QString& parameter_name, const QString& value)
{
    if (!fp->isOpen())
        return QFile::OpenError;

    QByteArray ba(parameter_name.toLatin1() + '=' + value.trimmed().toLatin1() + '\n');
    writeLine(ba, ba.length());

    return QFile::NoError;
}

/***************************************************************************
* Function : saveSetting (overloaded)
****************************************************************************/
QFile::FileError FileManager::saveSetting(const QString &parameter_name, const int& value)
{
    if (!fp->isOpen())
    {
        qDebug() << "File saving error";

        return QFile::OpenError;
    }

    QString value_str;
    value_str.setNum(value, 10);

    QByteArray ba(parameter_name.toLatin1() + '=' + value_str.toLatin1() + '\n');

    qDebug() << "Saving length line " << ba.length();

    writeLine(ba, ba.length());

    return QFile::NoError;
}

/***************************************************************************
* Function : loadSetting
****************************************************************************/
QFile::FileError FileManager::loadSetting(const QString &parameter_name, QString& value)
{
    QFile::FileError error = QFile::OpenError;
    bool found_value = false;
    QString line;

    if (!fp->isOpen())
    {
        return error;
    }

    value = NULL_STRING;

    seek(0);

    do
    {
         error = readLine(line);

        if (!line.isEmpty() && line.startsWith(parameter_name.trimmed()))
        {
            int pos = line.indexOf("=",0,Qt::CaseInsensitive);
            value = line.mid(pos+1, -1).trimmed();
            found_value = true;
            break;
        }
    }
    while ((error == QFile::NoError) || found_value);

    if (found_value)
        error = QFile::NoError;

    return error;
}

/***************************************************************************
* Function : loadSetting (overloaded)
****************************************************************************/
QFile::FileError FileManager::loadSetting(const QString &parameter_name, int& value)
{
    QFile::FileError error = QFile::OpenError;
    bool found_value = false, ok;
    QString line;

    if (!fp->isOpen())
        return error;

    value = 0;

    seek(0);

    do
    {
        error = readLine(line);

        if (!line.isEmpty() && line.startsWith(parameter_name.trimmed()))
        {
            int pos = line.indexOf("=",0,Qt::CaseInsensitive);
            value = line.mid(pos+1, -1).trimmed().toInt(&ok);
            found_value = true;
            break;
        }
    }
    while ((error == QFile::NoError) || found_value);

    if (found_value)
        error = QFile::NoError;

    return error;
}

/***************************************************************************
* Function : loadSetting (overloaded)
****************************************************************************/
QFile::FileError FileManager::loadSetting(const QString &parameter_name, quint32& value)
{
    QFile::FileError error = QFile::OpenError;
    bool found_value = false, ok;
    QString line;

    if (!fp->isOpen())
        return error;

    value = 0;

    seek(0);

    do
    {
        error = readLine(line);

        if (!line.isEmpty() && line.startsWith(parameter_name.trimmed()))
        {
            int pos = line.indexOf("=",0,Qt::CaseInsensitive);
            value = (quint32)line.mid(pos+1, -1).trimmed().toUInt(&ok);
            found_value = true;
            break;
        }
    }
    while ((error == QFile::NoError) || found_value);

    if (found_value)
        error = QFile::NoError;

    return error;
}

/***************************************************************************
* Function : loadSetting (overloaded)
****************************************************************************/
QFile::FileError FileManager::loadSetting(const QString& parameter_name, quint32& value,bool HexFlag)
{
    QFile::FileError error = QFile::OpenError;
    bool found_value = false, ok;
    QString line;

    if (!fp->isOpen())
        return error;

    value = 0;

    seek(0);

    do
    {
        error = readLine(line);

        if (!line.isEmpty() && line.startsWith(parameter_name.trimmed()))
        {
            int pos = line.indexOf("=",0,Qt::CaseInsensitive);

            if ( true == HexFlag ) {
                 value = (quint32)line.mid(pos+1, -1).trimmed().toUInt(&ok,16);
            }
            else {
                 value = (quint32)line.mid(pos+1, -1).trimmed().toUInt(&ok);
            } /* endif */

            found_value = true;
            break;
        }
    }
    while ((error == QFile::NoError) || found_value);

    if (found_value)
        error = QFile::NoError;

    return error;
}

