#include <QThread>
#include <QDebug>

#include "cqtopencvviewergl.h"

// ---
// Enable for advanced debug output
// ---
//#define VERBOSE_DEBUG
#undef VERBOSE_DEBUG

QGLFormat desiredFormat()
{
    QGLFormat fmt;

    fmt.setSwapInterval(1);

    return fmt;
}

CQtOpenCVViewerGl::CQtOpenCVViewerGl(QWidget *parent) :
    QGLWidget(parent)
{
    // Initialize globals
    mSceneChanged = false;
    mBgColor = QColor::fromRgb(150, 150, 150);

    mOutH = 0;
    mOutW = 0;
    mImgRatio = 4.0f/3.0f;

    mPosX = 0;
    mPosY = 0;

#if (defined VERBOSE_DEBUG)
    // Debug
    qDebug() << tr("CREATE : mPosX=%1 - mPosY=%2").arg(mPosX).arg(mPosY);
    qDebug() << tr("CREATE : Width=%1 - Height=%2").arg(this->size().width()).arg(this->size().height());
#endif // #if (defined VERBOSE_DEBUG)

    // Configure the timer
    connect(&timer, SIGNAL(timeout()), this, SLOT(updateGL()));

    if (format ().swapInterval () == -1)
    {
        // V_blank synchronization not available (tearing likely to happen)
        qDebug("Swap Buffers at v_blank not available: refresh at approx 60fps.");
        timer.setInterval (17);
    }
    else
    {
        // V_blank synchronization available
        timer.setInterval (0);
    }

    //timer.start ();
}

void CQtOpenCVViewerGl::initializeGL()
{
    makeCurrent();
    qglClearColor(mBgColor.darker());
}

void CQtOpenCVViewerGl::resizeGL(int width, int height)
{
    makeCurrent();
    glViewport(0, 0, (GLint)width, (GLint)height);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    glOrtho(0, width, 0, height, 0, 1);	// To Draw image in the center of the area

    glMatrixMode(GL_MODELVIEW);

    // ---> Scaled Image Sizes
    mOutH = width / mImgRatio;
    mOutW = width;

    if(mOutH > height)
    {
        mOutW = height * mImgRatio;
        mOutH = height;
    }

    emit imageSizeChanged( mOutW, mOutH );
    // <--- Scaled Image Sizes

    mPosX = (width - mOutW) / 2;
    mPosY = (height - mOutH) / 2;

#if (defined VERBOSE_DEBUG)
    qDebug() << tr("RESIZE IMAGE : mPosX=%1 - mPosY=%2").arg(mPosX).arg(mPosY);
#endif // #if (defined VERBOSE_DEBUG)

    mSceneChanged = true;

    updateScene();
}

void CQtOpenCVViewerGl::updateScene()
{
    if (mSceneChanged && this->isVisible())
    {
        updateGL ();
    }
}

void CQtOpenCVViewerGl::paintGL()
{
    makeCurrent();

    if (false == mSceneChanged)
    {
 //       return;
    }

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    renderImage();

    mSceneChanged = false;
}

void CQtOpenCVViewerGl::renderImage()
{
    makeCurrent();

    glClear(GL_COLOR_BUFFER_BIT);

    if (!mRenderQtImg.isNull())
    {
        glLoadIdentity();

        QImage image; // the image rendered

        glPushMatrix();

        {
            int imW = mRenderQtImg.width();
            int imH = mRenderQtImg.height();

            // The image is to be resized to fit the widget?
            if (imW != this->size().width() && imH != this->size().height())
            {
                image = mRenderQtImg.scaled (QSize(mOutW,mOutH),      //this->size(),
                                             Qt::KeepAspectRatio,
                                             Qt::FastTransformation);
            }
            else
            {
                image = mRenderQtImg;
            }

            // ---> Centering image in draw area
            //qDebug() << tr("CENTER IMAGE : mPosX=%1 - mPosY=%2").arg(mPosX).arg(mPosY);

            glRasterPos2i (mPosX, mPosY);
            // <--- Centering image in draw area

            imW = image.width();
            imH = image.height();

            glDrawPixels( imW, imH, GL_RGBA, GL_UNSIGNED_BYTE, image.bits());
        }

        glPopMatrix();

        // end
        glFinish ();
    }
}

void CQtOpenCVViewerGl::BlankImage (void)
{

}

bool CQtOpenCVViewerGl::showImage (cv::Mat image)
{
    // Check if we have a legit image, if not return
    if (false == image.data)
    {
        return false;
    }

    image.copyTo(mOrigImage);

    // Calculate ratio
    mImgRatio = (float)mOrigImage.cols/(float)mOrigImage.rows;

#if 0
    int tmpWidth, tmpHeight;
    int tmpPosX, tmpPosY;

    // Calculate new position for center image
    tmpWidth = this->size().width();
    tmpHeight = this->size().height();

    tmpPosX = (tmpWidth - mOrigImage.cols) / 2;
    tmpPosY = (tmpHeight - mOrigImage.rows) / 2;

    mPosX = tmpPosX;
    mPosY = tmpPosY;

    qDebug() << tr("SHOW IMAGE : mPosX=%1 - mPosY=%2 due to width,height=%3,%4 and image cols/rows=%5,%6").arg(tmpPosX).arg(tmpPosY).arg(tmpWidth).arg(tmpHeight).arg(mOrigImage.cols).arg(mOrigImage.rows);
#endif

    // Correctly display the image depending on the format it is
    if (mOrigImage.channels() == 3)
    {
        mRenderQtImg = QImage((const unsigned char*)(mOrigImage.data),
                              mOrigImage.cols, mOrigImage.rows,
                              mOrigImage.step, QImage::Format_RGB888).rgbSwapped();
    }
    else if (mOrigImage.channels() == 1)
    {
        mRenderQtImg = QImage((const unsigned char*)(mOrigImage.data),
                              mOrigImage.cols, mOrigImage.rows,
                              mOrigImage.step, QImage::Format_Indexed8);
    }
    else
    {
        return false;
    }

    // Convert image to GL format
    mRenderQtImg = QGLWidget::convertToGLFormat(mRenderQtImg);

    // The image is to be resized to fit the widget?
    if (mRenderQtImg.width() != this->size().width() && mRenderQtImg.height() != this->size().height())
    {
        mRenderQtImg = mRenderQtImg.scaled (QSize(this->size().width(), this->size().height()),
                                            Qt::KeepAspectRatio,
                                            Qt::FastTransformation);
    }

    // Update bottom left start point to have the image in the center of the scene
    mPosX = (this->size().width() - mRenderQtImg.width()) / 2;
    mPosY = (this->size().height() - mRenderQtImg.height()) / 2;

#if (defined VERBOSE_DEBUG)
    qDebug() << tr("SHOW IMAGE : mPosX=%1 - mPosY=%2 due to frame[W,H]=%3,%4 and image[W,H]=%5,%6").arg(mPosX).arg(mPosY).arg(this->size().width()).arg(this->size().height()).arg(mRenderQtImg.width()).arg(mRenderQtImg.height());
#endif // #if (defined VERBOSE_DEBUG)

    // Update scene
    mSceneChanged = true;

    updateScene ();

    return true;
}
