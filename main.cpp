#include <QApplication>
#include "cmainwindow.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QCoreApplication::setOrganizationName("Our");
    QCoreApplication::setOrganizationDomain("our.com");
    QCoreApplication::setApplicationName("Image Transformation");

    CMainWindow w;
    w.show();

    return a.exec();
}
