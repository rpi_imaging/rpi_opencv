#ifndef CQSETTINGS_H
#define CQSETTINGS_H

#include <QWidget>
#include <QSettings>

class CQsettings : public QWidget
{
    Q_OBJECT
public:
    explicit CQsettings(QString app, QString name, QString siteAdr = "", QWidget *parent = 0);
    ~CQsettings();

    void readSettings (QWidget* window);
    void writeSettings (QWidget* window);

private:
    void recurseRead (QSettings& settings, QObject* object);
    void recurseWrite (QSettings& settings, QObject* object);

    QString appName;
    QString companyName;
    QString companySite;

signals:

public slots:
};

#endif // CQSETTINGS_H
