﻿#include <QDebug>

#include "cqfilemanager.h"

#define MAX_FILE_LINE_LENGTH            255

CQfilemanager::CQfilemanager (QObject *parent) : QObject(parent)
{
    // Initialize globals
    currentLineLength = -1;
    currentLine = 0;
}

CQfilemanager::~CQfilemanager()
{

}

bool CQfilemanager::Resize (quint32 size)
{
    return fp->resize(size);
}

QFile::FileError CQfilemanager::Open (QString &filename, QFile::OpenMode mode, bool must_exist)
{
    QFile::FileError error = QFile::NoError;

    currentLine = 0;

    bool file_exist = QFile().exists(filename);

    if (false == file_exist && must_exist)
    {
        error = QFile::OpenError;
    }
    else
    {
        fp = new QFile(filename);

        if (!fp->open(mode))
        {
            error = QFile::OpenError;
        }
    }

    return error;
}

void CQfilemanager::Close (void)
{
    if (fp->handle() != -1)
    {
        fp->close();
    }

    fp = 0;
}

QFile::FileError CQfilemanager::Seek (quint32 position)
{
    if (false == fp->isOpen())
    {
        return QFile::OpenError;
    }

    if (position == 0)
    {
        currentLine = 0;
    }

    fp->seek((int)position);

    return QFile::NoError;
}

QFile::FileError CQfilemanager::SeekLine (quint32 line)
{
    QFile::FileError error = QFile::NoError;
    qint64 read_bytes = 0;
    quint32 i = 0;

    if (false == fp->isOpen())
    {
        error = QFile::OpenError;
        return error;
    }

    fp->seek(0);

    char temp[255];

    while ((i++ < line) && (read_bytes >= 0))
    {
        read_bytes = fp->readLine(temp, MAX_FILE_LINE_LENGTH);
    }

    i--;

    if ((i == line) && (read_bytes >= 0))
    {
        currentLine = line;
        error = QFile::NoError;
    }
    else
    {
        currentLine = 0;
        error = QFile::ReadError;
    }
    return error;
}

QFile::FileError CQfilemanager::WriteRaw (const char* data, quint32 size)
{
    qint64 opResult;

    if (false == fp->isOpen())
    {
        return QFile::OpenError;
    }

    opResult = fp->write (data, size);

    if (-1 == opResult)
    {
        qDebug () << "Failed to write setting to settings file";
    }

    return QFile::NoError;
}

QFile::FileError CQfilemanager::WriteLine (const char* data, quint32 size)
{
    qint64 opResult;

    if (false == fp->isOpen())
    {
        return QFile::OpenError;
    }

    opResult = fp->write (data, size);

    if (-1 == opResult)
    {
        qDebug () << "Failed to write setting to settings file";
    }

    return QFile::NoError;
}

QFile::FileError CQfilemanager::WriteLine (const char* data)
{
    const char terminator = '\n';

    if (false == fp->isOpen())
    {
        return QFile::OpenError;
    }

    fp->write (data, sizeof(data)+1);
    fp->write (&terminator,1);

    return QFile::NoError;
}

QFile::FileError CQfilemanager::ReadRaw (char* data, quint32 size)
{
    QFile::FileError return_code = QFile::NoError;

    if (false == fp->isOpen ())
    {
        return QFile::OpenError;
    }

    int ret = fp->read (data, size);

    // No error (0) or Only "n" bytes were read (>0)
    if (ret >= 0)
    {
        return_code = QFile::NoError;
    }
    else if (ret == -1)
    {
        // Nothing was read
        return_code = QFile::ReadError;
    }

    return return_code;
}

QFile::FileError CQfilemanager::ReadLine (char* data, quint32 size)
{
    QFile::FileError return_code = QFile::NoError;

    if (false == fp->isOpen())
    {
        currentLineLength = -1;

        return QFile::OpenError;
    }

    int ret = fp->readLine (data, size);

    // No error (0) or Only "n" bytes were read (>0)
    if (ret >= 0)
    {
        return_code = QFile::NoError;
        currentLine++;
    }
    else if (ret == -1)
    {
        // Nothing was read
        return_code = QFile::ReadError;
    }
    currentLineLength = ret;

    return return_code;
}

QFile::FileError CQfilemanager::ReadLine(QString &tmpStr)
{
    QFile::FileError return_code = QFile::NoError;
    char data[255] = {0};

    if (false == fp->isOpen ())
    {
        currentLineLength = -1;

        return QFile::OpenError;
    }

    int ret = fp->readLine (data, sizeof(data));

    tmpStr = QString(data).trimmed();

    // Skip Commented lines
    if (tmpStr.startsWith("//"))
    {
        tmpStr.clear ();
        ret = 0;
    }

    // Nothing was read (EOF reached) //or NULL string
    if (ret == -1)
    {
        return_code = QFile::ReadError;
    }
    else if (ret >= 0)
    {
        // No error (0) or Only "n" bytes were read (>0)
        return_code = QFile::NoError;
        currentLine++;
    }

    currentLineLength = ret;

    return return_code;
}

quint32 CQfilemanager::GetCurrentLineLength (void)
{
    return currentLineLength;
}

bool CQfilemanager::IsOpen (void)
{
    if (fp)
    {
        return fp->isOpen();
    }

    return false;
}

quint32 CQfilemanager::GetCurrentPos (void)
{
    return fp->pos ();
}

quint32 CQfilemanager::GetCurrentLine (void)
{
    if (currentLine > 0)
    {
        return (currentLine - 1);
    }

    return currentLine;
}

QStringList CQfilemanager::OpenFileDialog (QWidget *parent, QFileDialog::FileMode file_mode, QString &directory,
                                           const QString &window_title, const QString &filename_filter)
{
    QFileDialog *openFileDialog = new QFileDialog(parent);
    QStringList fileNames;

    openFileDialog->setFileMode(file_mode);
    openFileDialog->setDirectory(directory);
    openFileDialog->setWindowTitle(window_title);
    openFileDialog->setNameFilter(filename_filter);
    openFileDialog->exec();

    directory = openFileDialog->directory().absolutePath();

    fileNames = openFileDialog->selectedFiles();

    return fileNames;
}

QFile::FileError CQfilemanager::SaveSetting (const QString& parameter_name, const QString& value)
{
    if (false == fp->isOpen())
    {
        return QFile::OpenError;
    }

    QByteArray ba(parameter_name.toLatin1() + '=' + value.trimmed().toLatin1() + '\n');

    WriteLine(ba, ba.length());

    return QFile::NoError;
}

QFile::FileError CQfilemanager::SaveSetting (const QString &parameter_name, const int& value)
{
    if (false == fp->isOpen())
    {
        return QFile::OpenError;
    }

    QString value_str;
    value_str.setNum(value, 10);

    QByteArray ba(parameter_name.toLatin1() + '=' + value_str.toLatin1() + '\n');

    WriteLine(ba, ba.length());

    return QFile::NoError;
}

QFile::FileError CQfilemanager::LoadSetting (const QString &parameter_name, QString& value)
{
    QFile::FileError error = QFile::OpenError;
    bool found_value = false;
    QString line;

    if (false == fp->isOpen())
    {
        return error;
    }

    value.clear ();

    Seek (0);

    do
    {
         error = ReadLine(line);

        if (!line.isEmpty() && line.startsWith(parameter_name.trimmed()))
        {
            int pos = line.indexOf("=",0,Qt::CaseInsensitive);

            value = line.mid((pos + 1), -1).trimmed();

            found_value = true;

            break;
        }
    }
    while (error == QFile::NoError || true == found_value);

    if (true == found_value)
    {
        error = QFile::NoError;
    }

    return error;
}

QFile::FileError CQfilemanager::LoadSetting (const QString &parameter_name, int& value)
{
    QFile::FileError error = QFile::OpenError;
    bool found_value = false;
    QString line;

    if (false == fp->isOpen())
    {
        return error;
    }

    value = 0;

    Seek (0);

    do
    {
        error = ReadLine(line);

        if (!line.isEmpty() && line.startsWith(parameter_name.trimmed()))
        {
            int pos = line.indexOf("=",0,Qt::CaseInsensitive);
            value = line.mid(pos+1, -1).trimmed().toInt();
            found_value = true;
            break;
        }
    }
    while (error == QFile::NoError || true == found_value);

    if (true == found_value)
    {
        error = QFile::NoError;
    }

    return error;
}

QFile::FileError CQfilemanager::LoadSetting (const QString &parameter_name, quint32& value)
{
    QFile::FileError error = QFile::OpenError;
    bool found_value = false;
    QString line;

    if (false == fp->isOpen())
    {
        return error;
    }

    value = 0;

    Seek (0);

    do
    {
        error = ReadLine(line);

        if (!line.isEmpty() && line.startsWith(parameter_name.trimmed()))
        {
            int pos = line.indexOf("=",0,Qt::CaseInsensitive);

            value = (quint32)line.mid(pos+1, -1).trimmed().toUInt();

            found_value = true;

            break;
        }
    }
    while (error == QFile::NoError || true == found_value);

    if (true == found_value)
    {
        error = QFile::NoError;
    }

    return error;
}

QFile::FileError CQfilemanager::LoadSetting (const QString& parameter_name, quint32& value,bool HexFlag)
{
    QFile::FileError error = QFile::OpenError;
    bool found_value = false;
    QString line;

    if (!fp->isOpen())
    {
        return error;
    }

    value = 0;

    Seek (0);

    do
    {
        error = ReadLine(line);

        if (!line.isEmpty() && line.startsWith(parameter_name.trimmed()))
        {
            int pos = line.indexOf("=",0,Qt::CaseInsensitive);

            if ( true == HexFlag )
            {
                 value = (quint32)line.mid(pos+1, -1).trimmed().toUInt();
            }
            else
            {
                 value = (quint32)line.mid(pos+1, -1).trimmed().toUInt();
            }

            found_value = true;

            break;
        }
    }
    while (error == QFile::NoError || true == found_value);

    if (true == found_value)
    {
        error = QFile::NoError;
    }

    return error;
}

// End of file
