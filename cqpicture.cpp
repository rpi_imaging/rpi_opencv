﻿#include <stdio.h>
#include <QDebug>

#include "cqpicture.h"

using namespace cv;
using namespace CQpictureNamespace;

CQpicture::CQpicture(QObject *parent) : QObject(parent)
{
    // Initialize globals
    lastErrorDetail.clear ();

    imageIsLoaded  = false;    

    anchor.x = -1;
    anchor.y = -1;

    kernel = getStructuringElement (MORPH_RECT, Size (3, 3), anchor);
}

CQpicture::~CQpicture()
{

}

QString CQpicture::GetDetailedError (void)
{
    QString tmpStr;

    tmpStr = lastErrorDetail;

    lastErrorDetail.clear ();

    return tmpStr;
}

statusCodes CQpicture::SaveProcessedImage(QString imagePath)
{
    char* cstr;
    std::vector<int> params;
    int res = 0;

    // Copy QString to char*
    std::string fname = imagePath.toStdString();
    cstr = new char [fname.size()+1];
    strcpy (cstr, fname.c_str());

    // Define JPEG quality (0 to 100, higher is better)
#if (defined USE_OPENCV3)
    params.push_back(IMWRITE_JPEG_QUALITY);
#else
    params.push_back(CV_IMWRITE_JPEG_QUALITY);
#endif // #if (defined USE_OPENCV3)

    params.push_back(100);

    // Save image
    try
    {
        res = imwrite (cstr, processedImage, params);
    }
    catch (std::exception &e)
    {
        QString tmpStr = QString ("Error saving processed image %1 - %2").arg (res).arg (e.what ());

        qDebug () << tmpStr;

        return ERR_SAVE_FAILED;
    }

    return STATUS_NO_ERROR;
}

CQpictureNamespace::statusCodes CQpicture::LoadNewImage (QString imagePath)
{
    char* cstr;

    // Copy QString to char*
    std::string fname = imagePath.toStdString();
    cstr = new char [fname.size()+1];
    strcpy (cstr, fname.c_str());

    loadedImage.release ();

#if (defined USE_OPENCV3)
    loadedImage = imread (cstr, IMREAD_UNCHANGED);
#else
    loadedImage = imread (cstr, CV_LOAD_IMAGE_UNCHANGED);
#endif // #if (defined USE_OPENCV3)

    // Check error
    if (false == loadedImage.data)
    {
        imageIsLoaded  = false;

        return ERR_LOAD_FAILED;
    }

    imageIsLoaded  = true;   

    processedImage.release ();

    loadedImage.copyTo (processedImage);

    // Clear history
    picturesHistoryList.clear ();

    // Push image just loaded into history
    HistoryImagePush (loadedImage);

    return STATUS_NO_ERROR;
}

bool CQpicture::CompareImages (cv::Mat &firstImage, cv::Mat &secondImage, int threshold)
{
    Mat result;
    int similarPixels;

    compare (firstImage, secondImage, result, cv::CMP_EQ);
    similarPixels = cv::countNonZero (result);

    if (similarPixels  > threshold)
    {
        // Similar images
        return true;
    }

    // Different images
    return false;
}

bool CQpicture::CheckIfImagesAreIdentical (cv::Mat &firstImage, cv::Mat &secondImage)
{
    cv::Mat result;

    result = (firstImage == secondImage);

    if (cv::countNonZero (result))
    {
        // Identical images
        return true;
    }

    // Different images
    return false;
}

void CQpicture::GetLoadedImage (cv::Mat &image)
{
    loadedImage.copyTo (image);
}

void CQpicture::GetProcessedImage (cv::Mat &image)
{
    processedImage.copyTo (image);
}

void CQpicture::HistoryImagePush (cv::Mat &image)
{
    picturesHistoryList.append (image.clone ());
}

int CQpicture::GetHistorySize (void)
{
    return picturesHistoryList.size ();
}

CQpictureNamespace::statusCodes CQpicture::GetHistoryImageAt (int index, cv::Mat &image)
{
    int size;

    // Get the current size
    size = picturesHistoryList.size ();

    // Do the absolute value of the index to check the size
    // (we cannot use try...catch because QVector uses malloc that does not throw an exception
    // std::vector does
    index = abs (index);

    if (index < size)
    {
        picturesHistoryList.at (index).copyTo(image);
    }
    else
    {
        return ERR_INVALID_IMAGE;
    }

    return STATUS_NO_ERROR;
}

CQpictureNamespace::statusCodes CQpicture::ApplyColorSpaceConversion(int colorSpace, int numberOfChannels)
{
    // Check if we have an image loaded, if not return false
    if (false == imageIsLoaded)
    {
        return ERR_INVALID_IMAGE;
    }

    // Apply gray scale to the image
    try
    {
        // C++: void cvtColor(InputArray src, OutputArray dst, int code, int dstCn=0 )
        // Parameters:
        //     - input image    8-bit unsigned, 16-bit unsigned ( CV_16UC... ), or single-precision floating-point
        //     - dst            Output image of the same size and depth as src
        //     - code           Color space conversion code. The conventional ranges for R, G, and B channel values are:
        //                          - 0 to 255 for CV_8U images
        //                          - 0 to 65535 for CV_16U images
        //                          - 0 to 1 for CV_32F images
        //     - dstCn          Number of channels in the destination image;
        //                      if the parameter is 0, the number of the channels is derived automatically from src and code
        // Color space is from 0 to 135 with CV_RGB2GRAY = 7
        //processedImage *= 1./255;
        cvtColor (processedImage, processedImage, colorSpace, numberOfChannels);
    }
    catch (std::exception &e)
    {
        QString tmpStr = QString ("Error processing image with cvColor %1").arg (e.what ());

        lastErrorDetail = tmpStr;

        qDebug () << tmpStr;

        return ERR_INVALID_COLOR_CONV;
    }

    // Push image processed in the history
    HistoryImagePush (processedImage);

    return STATUS_NO_ERROR;
}

CQpictureNamespace::statusCodes CQpicture::ApplyThreshold (int thresholdValue, int maxVal, int thresholdType, bool applyOtsu)
{
    int imageType;

    // Check if we have an image loaded, if not return false
    if (false == imageIsLoaded)
    {
        return ERR_INVALID_IMAGE;
    }

    if (true == applyOtsu)
    {
        thresholdType += THRESH_OTSU;

        // If we would like to apply OTSU we must be sure that the image type is 8 bit (from 0 to 25)
        // Here a list of the image types supported by OpenCv (type \ # of channels):
        //          C1  C2  C3  C4
        // CV_8U	0	8	16	24
        // CV_8S	1	9	17	25
        // CV_16U	2	10	18	26
        // CV_16S	3	11	19	27
        // CV_32S	4	12	20	28
        // CV_32F	5	13	21	29
        // CV_64F	6	14	22	30
        imageType = processedImage.type ();

        if (0 != imageType && 1 != imageType)
        {
            return ERR_INVALID_IMAGE_TYPE;
        }
    }

    // Apply threshold
    try
    {
        threshold (processedImage, processedImage, thresholdValue, maxVal, thresholdType);
    }
    catch (std::exception &e)
    {
        QString tmpStr = QString ("Error processing image with cvColor %1").arg (e.what ());

        lastErrorDetail = tmpStr;

        qDebug () << tmpStr;

        return ERR_GENERIC_FAILURE;
    }

    // Push image processed in the history
    HistoryImagePush (processedImage);

    return STATUS_NO_ERROR;
}

CQpictureNamespace::statusCodes CQpicture::ApplyMorphologyEx (int structuringElemShape, int morphSize, int morphType, int iterationsNumber)
{
    // Create a structuring element (SE)
    // Get the structuring element, shape could be:
    // - MORPH_RECT, MORPH_CROSS or MORPH_ELLIPSE
    Mat tmpKernel = getStructuringElement (structuringElemShape,
                                           Size ((4 * morphSize + 1), (2 * morphSize + 1)),
                                           Point (morphSize, morphSize));

    try
    {
        // Morphologic transformation
        // Type of a morphological operation that can be one of the following:
        // - MORPH_OPEN, MORPH_CLOSE, MORPH_GRADIENT, MORPH_TOPHAT, MORPH_BLACKHAT
        morphologyEx (processedImage, processedImage, morphType, tmpKernel, Point(-1,-1), iterationsNumber);
    }
    catch (std::exception &e)
    {
        QString tmpStr = QString ("Error processing image with MorphologyEx %1").arg (e.what ());

        lastErrorDetail = tmpStr;

        qDebug () << tmpStr;

        return ERR_GENERIC_FAILURE;
    }

    // Push image processed in the history
    HistoryImagePush (processedImage);

    return STATUS_NO_ERROR;
}

CQpictureNamespace::statusCodes CQpicture::ApplySubtract (CQpictureNamespace::subtractOp op, cv::Mat otherImage)
{
    InputArray mask = noArray ();
    int dtype = -1;

    try
    {
        if (SUBTRACT_TO == op)
        {
            subtract (otherImage, processedImage, processedImage, mask, dtype);
        }
        else
        {
            subtract (processedImage, otherImage, processedImage, mask, dtype);
        }
    }
    catch (std::exception &e)
    {
        QString tmpStr = QString ("Error processing image with subtract %1").arg (e.what ());

        lastErrorDetail = tmpStr;

        qDebug () << tmpStr;

        return ERR_INVALID_VECTOR_SIZES;
    }

    // Push image processed in the history
    HistoryImagePush (processedImage);

    return STATUS_NO_ERROR;
}

CQpictureNamespace::statusCodes CQpicture::ApplyDilate (int structuringElemShape, int morphSize, int iterationsNumber)
{
    // Create a structuring element (SE)
    // Get the structuring element, shape could be:
    // - MORPH_RECT, MORPH_CROSS or MORPH_ELLIPSE
    Mat tmpKernel = getStructuringElement (structuringElemShape,
                                           Size ((4 * morphSize + 1), (2 * morphSize + 1)),
                                           Point (morphSize, morphSize));

    try
    {
        // Apply dilate
        dilate (processedImage, processedImage, tmpKernel, Point (morphSize, morphSize), iterationsNumber);
    }
    catch (std::exception &e)
    {
        QString tmpStr = QString ("Error processing image with dilate %1").arg (e.what ());

        lastErrorDetail = tmpStr;

        qDebug () << tmpStr;

        return ERR_GENERIC_FAILURE;
    }

    // Push image processed in the history
    HistoryImagePush (processedImage);

    return STATUS_NO_ERROR;
}

CQpictureNamespace::statusCodes CQpicture::ApplyDistanceTransform (int distanceType, cv::Mat &labels2DArray, int maskSize, int labelType)
{
    // C++: void distanceTransform (InputArray src, OutputArray dst, int distanceType, int maskSize, int dstType=CV_32F)
    // C++: void distanceTransform  (InputArray src, OutputArray dst, OutputArray labels, int distanceType, int maskSize, int labelType=DIST_LABEL_CCOMP)
    // Parameters:
    //   - src              8-bit, single-channel (binary) source image
    //   - dst              Output image with calculated distances.
    //                      It is a 8-bit or 32-bit floating-point, single-channel image of the same size as src
    //   - distanceType     Type of distance. It can be CV_DIST_L1, CV_DIST_L2, or CV_DIST_C
    //   - maskSize         Size of the distance transform mask. It can be 3, 5, or CV_DIST_MASK_PRECISE
    //                      (the latter option is only supported by the first function).
    //                      In case of the CV_DIST_L1 or CV_DIST_C distance type, the parameter is forced to 3 because a  3x3 mask gives the same result as
    //                      5x5 or any larger aperture
    //   - dstType          Type of output image. It can be CV_8U or CV_32F. Type CV_8U can be used only for the first variant of the function
    //                      and distanceType == CV_DIST_L1
    //   - labels           Optional output 2D array of labels (the discrete Voronoi diagram). It has the type CV_32SC1 and the same size as src
    //   - labelType        Type of the label array to build. If labelType==DIST_LABEL_CCOMP then each connected component of zeros in src
    //                      (as well as all the non-zero pixels closest to the connected component) will be assigned the same label.
    //                      If labelType==DIST_LABEL_PIXEL then each zero pixel (and all the non-zero pixels closest to it) gets its own label
    if (DIST_LABEL_CCOMP == labelType || DIST_LABEL_PIXEL == labelType)
    {
        try
        {
            // Builds the discrete Voronoi diagram
            distanceTransform (processedImage, processedImage, labels2DArray, distanceType, maskSize, labelType);
        }
        catch (std::exception &e)
        {
            QString tmpStr = QString ("Error processing image with distance transform (Voronoi form) %1").arg (e.what ());

            lastErrorDetail = tmpStr;

            qDebug () << tmpStr;

            return ERR_GENERIC_FAILURE;
        }
    }
    else
    {
        try
        {
            // Computes the distance transform map
            distanceTransform (processedImage, processedImage, distanceType, maskSize);
        }
        catch (std::exception &e)
        {
            QString tmpStr = QString ("Error processing image with distance transform %1").arg (e.what ());

            lastErrorDetail = tmpStr;

            qDebug () << tmpStr;

            return ERR_GENERIC_FAILURE;
        }
    }

    // Push image processed in the history
    HistoryImagePush (processedImage);

    return STATUS_NO_ERROR;
}

CQpictureNamespace::statusCodes CQpicture::ApplyWatershed (cv::Mat &markersArray)
{
    // C++: void watershed(InputArray image, InputOutputArray markers)
    // Parameters:
    //    - image       Input 8-bit 3-channel image
    //    - markers     Input/output 32-bit single-channel image (map) of markers. It should have the same size as image
    try
    {
        watershed (processedImage, markersArray);
    }
    catch (std::exception &e)
    {
        QString tmpStr = QString ("Error processing image with watershed %1").arg (e.what ());

        lastErrorDetail = tmpStr;

        qDebug () << tmpStr;

        return ERR_GENERIC_FAILURE;
    }

    // Push image processed in the history
    HistoryImagePush (processedImage);

    return STATUS_NO_ERROR;
}

CQpictureNamespace::statusCodes CQpicture::ApplyConnectedComponents (int connectivity, int ltype)
{
    // C++: int connectedComponents(InputArray image, OutputArray labels, int connectivity=8, int ltype=CV_32S)
    // Parameters:
    //    - image           The image to be labeled
    //    - labels          Destination labeled image
    //    - connectivity    8 or 4 for 8-way or 4-way connectivity respectively
    //    - ltype           Output image label type. Currently CV_32S and CV_16U are supported
    try
    {
        connectedComponents (processedImage, processedImage, connectivity, ltype);
    }
    catch (std::exception &e)
    {
        QString tmpStr = QString ("Error processing image with connected components %1").arg (e.what ());

        lastErrorDetail = tmpStr;

        qDebug () << tmpStr;

        return ERR_GENERIC_FAILURE;
    }

    // Push image processed in the history
    HistoryImagePush (processedImage);

    return STATUS_NO_ERROR;
}

void CQpicture::ResetProcessedImage (void)
{
    // Re-load original image into processed
    loadedImage.copyTo (processedImage);

    // Clear history
    picturesHistoryList.clear ();
}

bool CQpicture::GetProcessedImageData (int &depth, int &chNum, int &type)
{
    bool res = false;

    if (processedImage.data)
    {
        chNum = processedImage.channels();
        depth = processedImage.depth();
        type = processedImage.type();

        res = true;
    }

    return res;
}

bool CQpicture::GetLoadedImageData (int &depth, int &chNum, int &type)
{
    bool res = false;

    if (loadedImage.data)
    {
        chNum = loadedImage.channels();
        depth = loadedImage.depth();
        type = loadedImage.type();

        res = true;
    }

    return res;
}

// End of file
