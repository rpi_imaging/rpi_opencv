; -- ImageTransformation.iss --
#define AppName                         "OpenCV Image Trasformation"
#define OutputSetupAppName              "OpenCV Image Trasformation"
#define VersionType                     ""
#define VersionNumber                   "0.7.0"
#define AppVersion                      VersionNumber+VersionType
#define SPACE                           " "
#define V                               "v"
#define ExecName                        "ImageTransformation.exe"

#define LibrariesDir                    "libraries"
#define DocsDir                         "docs"
#define ImagePluginDir                  "imageformats"
#define PlatformsPluginDir              "platforms"
#define SettingsDir                     "settings"
#define LogsDir                         "logs"   
#define ImagesDir                       "images"                        

#define MyDateTimeString 				        GetDateTimeString('dd_mm_yyyy', '', '');

[Code]
procedure InitializeWizard();
var
  AboutLabel: TNewStaticText;
begin
  { Other custom controls }
  AboutLabel := TNewStaticText.Create(WizardForm);
  AboutLabel.Font.Size := 7;
  AboutLabel.Font.Name := 'Times New Romans';
  AboutLabel.Caption := 'Developed by Our srl';
  AboutLabel.Parent := WizardForm;
  AboutLabel.Top := 325;
  AboutLabel.Left := 5;
end;

function InitializeUninstall(): Boolean;
begin
  Result := MsgBox( 'Warning this operation will reset Control Application appearance to its default value. Do you really want to proceed?', mbConfirmation, MB_YESNO) = idYes;
  if Result = False then
    MsgBox( 'Ok, Uninstall is aborted.', mbInformation, MB_OK);
end;

[Setup]
; NOTE: The value of AppId uniquely identifies this application.
; Do not use the same AppId value in installers for other applications.
; (To generate a new GUID, click Tools | Generate GUID inside the IDE.)
AppId={{67F2A324-8747-4601-9CDB-9925BC3076EF}}

AppName={#AppName}{#SPACE}{#V}{#AppVersion}
AppVerName={#AppName}{#SPACE}{#V}{#AppVersion}
DefaultDirName={pf}\{#AppName}{#SPACE}{#V}{#AppVersion}
DefaultGroupName={#AppName}{#SPACE}{#V}{#AppVersion}
UninstallDisplayIcon={app}\{#ExecName}
AppPublisher=Our srl
AppPublisherURL=http://www.our.com/
AppVersion={#VersionNumber}
AppCopyright=Copyright (C) Our srl
VersionInfoCopyright=Copyright (C) 2015 Our srl
VersionInfoProductName={#AppName}{#SPACE}{#V}{#AppVersion}{#SPACE}{#VersionType}
AppSupportPhone=+393453117124
AppContact=alberto.saviotti@gmail.com
VersionInfoVersion={#VersionNumber}
OutputBaseFilename={#OutputSetupAppName}
OutputDir=.
WizardSmallImageFile="..\images\logo.bmp"
PrivilegesRequired=admin

[Tasks]
name: startupicon; Description: Run {#AppName} minimized automatically at Windows startup; GroupDescription: Additional tasks:; Flags: unchecked

[Dirs]
Name:"{app}\"; Permissions:everyone-modify
Name: {app}\{#LibrariesDir}; Permissions:everyone-modify
Name: {app}\{#PlatformsPluginDir}; Permissions:everyone-modify
Name: {app}\{#ImagePluginDir}; Permissions:everyone-modify
Name: {app}\{#SettingsDir}; Permissions:everyone-modify
Name: {app}\{#DocsDir}; Permissions:everyone-modify
Name: {app}\{#LogsDir}; Permissions:everyone-modify
Name: {app}\{#ImagesDir}; Permissions:everyone-modify

[Files]
; Executable
Source: ..\release\{#ExecName}; DestDir: {app}; DestName: {#ExecName}

; ---------------------------------------------------------------------
;  Libraries files
; ---------------------------------------------------------------------
; Qt libraries
Source: ".\prerequisites\Qt\Qt5Core.dll"; DestDir: "{app}"
Source: ".\prerequisites\Qt\Qt5Gui.dll"; DestDir: "{app}"
Source: ".\prerequisites\Qt\Qt5Network.dll"; DestDir: "{app}"
Source: ".\prerequisites\Qt\Qt5Multimedia.dll"; DestDir: "{app}"
Source: ".\prerequisites\Qt\Qt5PrintSupport.dll"; DestDir: "{app}"
Source: ".\prerequisites\Qt\Qt5Widgets.dll"; DestDir: "{app}"
Source: ".\prerequisites\Qt\Qt5OpenGL.dll"; DestDir: "{app}"
Source: ".\prerequisites\Qt\Qt5Test.dll"; DestDir: "{app}"
Source: ".\prerequisites\Qt\icuin53.dll"; DestDir: "{app}"
Source: ".\prerequisites\Qt\icuuc53.dll"; DestDir: "{app}"
Source: ".\prerequisites\Qt\icudt53.dll"; DestDir: "{app}"

; OpenCV libraries
Source: ".\prerequisites\opencv\libopencv_core2410.dll"; DestDir: "{app}"
Source: ".\prerequisites\opencv\libopencv_highgui2410.dll"; DestDir: "{app}"
Source: ".\prerequisites\opencv\libopencv_imgproc2410.dll"; DestDir: "{app}"
Source: ".\prerequisites\opencv\libopencv_objdetect2410.dll"; DestDir: "{app}"

Source: ".\prerequisites\opencv\libopencv_highgui300.dll"; DestDir: "{app}"
Source: ".\prerequisites\opencv\libopencv_core300.dll"; DestDir: "{app}"
Source: ".\prerequisites\opencv\libopencv_imgcodecs300.dll"; DestDir: "{app}"
Source: ".\prerequisites\opencv\libopencv_imgproc300.dll"; DestDir: "{app}"
Source: ".\prerequisites\opencv\libopencv_objdetect300.dll"; DestDir: "{app}"
Source: ".\prerequisites\opencv\libopencv_videoio300.dll"; DestDir: "{app}"
Source: ".\prerequisites\opencv\libopencv_ml300.dll"; DestDir: "{app}"

; MinGW libraries
Source: ".\prerequisites\mingw\libgcc_s_dw2-1.dll"; DestDir: "{app}"
Source: ".\prerequisites\mingw\libwinpthread-1.dll"; DestDir: "{app}"
Source: ".\prerequisites\mingw\libstdc++-6.dll"; DestDir: "{app}"

; TBB
Source: ".\prerequisites\tbb\tbb.dll"; DestDir: "{app}"

; ---------------------------------------------------------------------
;  Qt plugins
; ---------------------------------------------------------------------
Source: ".\prerequisites\Qt\imageformats\qdds.dll"; DestDir: "{app}\{#ImagePluginDir}"
Source: ".\prerequisites\Qt\imageformats\qgif.dll"; DestDir: "{app}\{#ImagePluginDir}"
Source: ".\prerequisites\Qt\imageformats\qicns.dll"; DestDir: "{app}\{#ImagePluginDir}"
Source: ".\prerequisites\Qt\imageformats\qico.dll"; DestDir: "{app}\{#ImagePluginDir}"
Source: ".\prerequisites\Qt\imageformats\qjp2.dll"; DestDir: "{app}\{#ImagePluginDir}"
Source: ".\prerequisites\Qt\imageformats\qjpeg.dll"; DestDir: "{app}\{#ImagePluginDir}"
Source: ".\prerequisites\Qt\imageformats\qmng.dll"; DestDir: "{app}\{#ImagePluginDir}"
Source: ".\prerequisites\Qt\imageformats\qsvg.dll"; DestDir: "{app}\{#ImagePluginDir}"
Source: ".\prerequisites\Qt\imageformats\qtga.dll"; DestDir: "{app}\{#ImagePluginDir}"
Source: ".\prerequisites\Qt\imageformats\qtiff.dll"; DestDir: "{app}\{#ImagePluginDir}"
Source: ".\prerequisites\Qt\imageformats\qwbmp.dll"; DestDir: "{app}\{#ImagePluginDir}"
Source: ".\prerequisites\Qt\imageformats\qwebp.dll"; DestDir: "{app}\{#ImagePluginDir}"

Source: ".\prerequisites\Qt\platforms\qwindows.dll"; DestDir: "{app}\{#PlatformsPluginDir}"

; ---------------------------------------------------------------------
;  Docs files
; ---------------------------------------------------------------------
Source: "..\docs\ImageTransformation.pdf"; DestDir: "{app}\{#DocsDir}"

; ---------------------------------------------------------------------
;  Images
; ---------------------------------------------------------------------
Source: "..\images\*.*"; DestDir: "{app}\{#ImagesDir}"

; ---------------------------------------------------------------------
;  Configuration and settings files
; ---------------------------------------------------------------------
Source: "..\settings\settings.cfg"; DestDir: "{app}\{#SettingsDir}"
Source: "..\settings\windows_position.cfg"; DestDir: "{app}\{#SettingsDir}"

[Run]
Filename: {app}\{#ExecName}; Description: "Launch application"; Flags: postinstall nowait skipifsilent unchecked

[UninstallDelete]
Type: filesandordirs; Name: {app}

[Registry]
Root: HKCU; Subkey: Software\Our\Qt Control Application; Flags: uninsdeletekey

[Icons]
Name: {group}\{#AppName}; Filename: {app}\{#ExecName}; WorkingDir: {app}
Name: {group}\Uninstall{#SPACE}{#AppName}; Filename: {app}\unins000.exe; WorkingDir: {app}
Name: {commondesktop}\{#AppName}{#SPACE}{#V}{#AppVersion}; Filename: {app}\{#ExecName}; WorkingDir: {app}

; Startup shortcut (if selected by user)
Name: {userstartup}\{#AppName}; Filename: {app}\{#ExecName}; Parameters: /MINIMIZED; Tasks: startupicon; workingdir: {app}

; Create the application shortcut in the installation folder that will be used to run it at startup when required
Name: {app}\{#AppName}; Filename: {app}\{#ExecName}; Parameters: /MINIMIZED; workingdir: {app}
