﻿#ifndef CMAINWINDOW_H
#define CMAINWINDOW_H

#include <QMainWindow>
#include <QLabel>

#include <opencv2/core/core.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/videoio.hpp>

#include "cqtopencvviewergl.h"
#include "cqpicture.h"
#include "cqfilemanager.h"
#include "cqsettings.h"

namespace Ui {
    class CMainWindow;
}

class CMainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit CMainWindow(QWidget *parent = 0);
    ~CMainWindow();

protected:
    virtual void timerEvent(QTimerEvent *);

private slots:
    void on_actionStart_triggered();

    void on_actionStop_triggered();

    void on_checkBox_fullFace_clicked();

    void on_pushButton_applyGreyScale_clicked();

    void on_actionReset_triggered();

    void on_actionLoad_Image_triggered();

    void on_actionOpen_Howto_triggered();

    void on_actionSave_processed_image_triggered();

    void on_pushButton_applyMorphologyEx_clicked();

    void on_actionLoad_and_Execute_script_triggered();

    void on_pushButton_applyDilate_clicked();

    void on_pushButton_applyThreshold_clicked();

    void on_pushButton_applyDistanceTransform_clicked();

    void on_pushButton_applySubtract_clicked();

    void on_comboBox_distanceTransformType_currentIndexChanged(const QString &arg1);

    void on_comboBox_distanceTransformLabelType_currentIndexChanged(const QString &arg1);

    void on_verticalSlider_history_valueChanged(int value);

    void on_comboBox_subtractSrc1_currentIndexChanged(int index);

    void on_comboBox_subtractSrc2_currentIndexChanged(int index);

    void on_pushButton_applyWatershed_clicked();

    void on_comboBox_subtractResult_currentIndexChanged(int index);

    void on_pushButton_applyConnectedComponents_clicked();

    void on_pushButton_getDetailedError_clicked();

    void on_actionWatersheed_triggered();

private:
    struct CQcoordinates
    {
        int xTopLeft;
        int xBottomRight;
        int yTopLeft;
        int yBottomRigth;
    };

    CQcoordinates coordinates_left;
    CQcoordinates coordinates_right;
    CQcoordinates coordinates_history;

    Ui::CMainWindow *ui;

    int mCameraEventId;

    cv::Mat mOrigImage;
    cv::Mat mElabImage;
    cv::Mat mHistoryImage;

    cv::VideoCapture mCapture;

    // ---> Face detectors
    cv::CascadeClassifier mFaceDetector;
    cv::CascadeClassifier mEyeDetector;
    cv::CascadeClassifier mMouthDetector;
    cv::CascadeClassifier mNoseDetector;
    // <--- Face detectors

    // Settings file
    CQsettings *widgetSettings;

    void LoadSettings (void);
    void SaveSettings (void);       

    // Load image
    void DisplayImage (void);

    // Picture management
    QString pictureName_left;
    QString pictureName_right;

    CQpicture *picture_left;
    CQpicture *picture_right;

    // Currently selected camera widget, picture and label
    CQtOpenCVViewerGl *currentlySelectedCameraWidget;
    CQpicture *currentlySelectedPicture;
    QLabel *currentlySelectedImageLabel;

    // UI manipulation
    void StatusBarMessage (QString msg);

    bool eventFilter (QObject *obj, QEvent *event);

    void CheckWidgets (void);

    void UpdateHistoryItems (void);

    void UpdateCurrentImageLabel (QString str);
};

#endif // CMAINWINDOW_H
