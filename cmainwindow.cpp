﻿#include "cmainwindow.h"
#include "ui_cmainwindow.h"

#include <stdlib.h>
#include <vector>
#include <QDebug>
#include <QFileDialog>
#include <QDesktopServices>
#include <QMouseEvent>

#define FEAT_FACE_FILE                  "haarcascade_frontalface_default.xml"
#define FEAT_EYE_FILE                   "haarcascade_mcs_eyepair_big.xml"
#define FEAT_NOSE_FILE                  "haarcascade_mcs_nose.xml"
#define FEAT_MOUTH_FILE                 "haarcascade_mcs_mouth.xml"

using namespace std;

#define APP_TITLE_VERSION               " - v. 0.7.0"

#define APP_FILE_SETTING                "/settings/settings.cfg"

#define APP_SETTING_LAST_LEFT_PICTURE   "LAST_LEFT_PICTURE"
#define APP_SETTING_LAST_RIGHT_PICTURE  "LAST_RIGHT_PICTURE"

#define LEFT_IMAGE_INDEX_VALUE          0
#define RIGHT_IMAGE_INDEX_VALUE         1

namespace
{
    enum pictureListEnum
    {
        PICTURE_LEFT  = 0,
        PICTURE_RIGHT = 1
    };

    typedef struct
    {
        CQpictureNamespace::colorCodes code;
        QString text;
    } colorConversionDataTy;

    colorConversionDataTy colorConversionData[] =
    {
        {CQpictureNamespace::COLOR_BGR2BGRA, "COLOR_BGR2BGRA (0)"},
        {CQpictureNamespace::COLOR_RGB2RGBA, "COLOR_RGB2RGBA (0)"},
        {CQpictureNamespace::COLOR_BGRA2BGR, "COLOR_BGRA2BGR (1)"},
        {CQpictureNamespace::COLOR_RGBA2RGB, "COLOR_RGBA2RGB (1)"},
        {CQpictureNamespace::COLOR_BGR2RGBA, "COLOR_BGR2RGBA (2)"},
        {CQpictureNamespace::COLOR_RGB2BGRA, "COLOR_RGB2BGRA (2)"},
        {CQpictureNamespace::COLOR_RGBA2BGR, "COLOR_RGBA2BGR (3)"},
        {CQpictureNamespace::COLOR_BGRA2RGB, "COLOR_BGRA2RGB (3)"},
        {CQpictureNamespace::COLOR_BGR2RGB, "COLOR_BGR2RGB (4)"},
        {CQpictureNamespace::COLOR_RGB2BGR, "COLOR_RGB2BGR (4)"},
        {CQpictureNamespace::COLOR_BGRA2RGBA, "COLOR_BGRA2RGBA (5)"},
        {CQpictureNamespace::COLOR_RGBA2BGRA, "COLOR_RGBA2BGRA (5)"},
        {CQpictureNamespace::COLOR_BGR2GRAY, "COLOR_BGR2GRAY (6)"},
        {CQpictureNamespace::COLOR_RGB2GRAY, "COLOR_RGB2GRAY (7)"},
        {CQpictureNamespace::COLOR_GRAY2BGR, "COLOR_GRAY2BGR (8)"},
        {CQpictureNamespace::COLOR_GRAY2RGB, "COLOR_GRAY2RGB (8)"},
        {CQpictureNamespace::COLOR_GRAY2BGRA, "COLOR_GRAY2BGRA (9)"},
        {CQpictureNamespace::COLOR_GRAY2RGBA, "COLOR_GRAY2RGBA (9)"},
        {CQpictureNamespace::COLOR_BGRA2GRAY, "COLOR_BGRA2GRAY (10)"},
        {CQpictureNamespace::COLOR_RGBA2GRAY, "COLOR_RGBA2GRAY (11)"},
        {CQpictureNamespace::COLOR_BGR2BGR565, "COLOR_BGR2BGR565 (12)"},
        {CQpictureNamespace::COLOR_RGB2BGR565, "COLOR_RGB2BGR565 (13)"},
        {CQpictureNamespace::COLOR_BGR5652BGR, "COLOR_BGR5652BGR (14)"},
        {CQpictureNamespace::COLOR_BGR5652RGB, "COLOR_BGR5652RGB (15)"},
        {CQpictureNamespace::COLOR_BGRA2BGR565, "COLOR_BGRA2BGR565 (16)"},
        {CQpictureNamespace::COLOR_RGBA2BGR565, "COLOR_RGBA2BGR565 (17)"},
        {CQpictureNamespace::COLOR_BGR5652BGRA, "COLOR_BGR5652BGRA (18)"},
        {CQpictureNamespace::COLOR_BGR5652RGBA, "COLOR_BGR5652RGBA (19)"},
        {CQpictureNamespace::COLOR_GRAY2BGR565, "COLOR_GRAY2BGR565 (20)"},
        {CQpictureNamespace::COLOR_BGR5652GRAY, "COLOR_BGR5652GRAY (21)"},
        {CQpictureNamespace::COLOR_BGR2BGR555, "COLOR_BGR2BGR555 (22)"},
        {CQpictureNamespace::COLOR_RGB2BGR555, "COLOR_RGB2BGR555 (23)"},
        {CQpictureNamespace::COLOR_BGR5552BGR, "COLOR_BGR5552BGR (24)"},
        {CQpictureNamespace::COLOR_BGR5552RGB, "COLOR_BGR5552RGB (25)"},
        {CQpictureNamespace::COLOR_BGRA2BGR555, "COLOR_BGRA2BGR555 (26)"},
        {CQpictureNamespace::COLOR_RGBA2BGR555, "COLOR_RGBA2BGR555 (27)"},
        {CQpictureNamespace::COLOR_BGR5552BGRA, "COLOR_BGR5552BGRA (28)"},
        {CQpictureNamespace::COLOR_BGR5552RGBA, "COLOR_BGR5552RGBA (29)"},
        {CQpictureNamespace::COLOR_GRAY2BGR555, "COLOR_GRAY2BGR555 (30)"},
        {CQpictureNamespace::COLOR_BGR5552GRAY, "COLOR_BGR5552GRAY (31)"},
        {CQpictureNamespace::COLOR_BGR2XYZ, "COLOR_BGR2XYZ (32)"},
        {CQpictureNamespace::COLOR_RGB2XYZ, "COLOR_RGB2XYZ (33)"},
        {CQpictureNamespace::COLOR_XYZ2BGR, "COLOR_XYZ2BGR (34)"},
        {CQpictureNamespace::COLOR_XYZ2RGB, "COLOR_XYZ2RGB (35)"},
        {CQpictureNamespace::COLOR_BGR2YCrCb, "COLOR_BGR2YCrCb (36)"},
        {CQpictureNamespace::COLOR_RGB2YCrCb, "COLOR_RGB2YCrCb (37)"},
        {CQpictureNamespace::COLOR_YCrCb2BGR, "COLOR_YCrCb2BGR (38)"},
        {CQpictureNamespace::COLOR_YCrCb2RGB, "COLOR_YCrCb2RGB (39)"},
        {CQpictureNamespace::COLOR_BGR2HSV, "COLOR_BGR2HSV (40)"},
        {CQpictureNamespace::COLOR_RGB2HSV, "COLOR_RGB2HSV (41)"},
        {CQpictureNamespace::COLOR_BGR2Lab, "COLOR_BGR2Lab (44)"},
        {CQpictureNamespace::COLOR_RGB2Lab, "COLOR_RGB2Lab (45)"},
        {CQpictureNamespace::COLOR_BGR2Luv, "COLOR_BGR2Luv (50)"},
        {CQpictureNamespace::COLOR_RGB2Luv, "COLOR_RGB2Luv (51)"},
        {CQpictureNamespace::COLOR_BGR2HLS, "COLOR_BGR2HLS (52)"},
        {CQpictureNamespace::COLOR_RGB2HLS, "COLOR_RGB2HLS (53)"},
        {CQpictureNamespace::COLOR_HSV2BGR, "COLOR_HSV2BGR (54)"},
        {CQpictureNamespace::COLOR_HSV2RGB, "COLOR_HSV2RGB (55)"},
        {CQpictureNamespace::COLOR_Lab2BGR, "COLOR_Lab2BGR (56)"},
        {CQpictureNamespace::COLOR_Lab2RGB, "COLOR_Lab2RGB (57)"},
        {CQpictureNamespace::COLOR_Luv2BGR, "COLOR_Luv2BGR (58)"},
        {CQpictureNamespace::COLOR_Luv2RGB, "COLOR_Luv2RGB (59)"},
        {CQpictureNamespace::COLOR_HLS2BGR, "COLOR_HLS2BGR (60)"},
        {CQpictureNamespace::COLOR_HLS2RGB, "COLOR_HLS2RGB (61)"},
        {CQpictureNamespace::COLOR_BGR2HSV_FULL, "COLOR_BGR2HSV_FULL (66)"},
        {CQpictureNamespace::COLOR_RGB2HSV_FULL, "COLOR_RGB2HSV_FULL (67)"},
        {CQpictureNamespace::COLOR_BGR2HLS_FULL, "COLOR_BGR2HLS_FULL (68)"},
        {CQpictureNamespace::COLOR_RGB2HLS_FULL, "COLOR_RGB2HLS_FULL (69)"},
        {CQpictureNamespace::COLOR_HSV2BGR_FULL, "COLOR_HSV2BGR_FULL (70)"},
        {CQpictureNamespace::COLOR_HSV2RGB_FULL, "COLOR_HSV2RGB_FULL (71)"},
        {CQpictureNamespace::COLOR_HLS2BGR_FULL, "COLOR_HLS2BGR_FULL (72)"},
        {CQpictureNamespace::COLOR_HLS2RGB_FULL, "COLOR_HLS2RGB_FULL (73)"},
        {CQpictureNamespace::COLOR_LBGR2Lab, "COLOR_LBGR2Lab (74)"},
        {CQpictureNamespace::COLOR_LRGB2Lab, "COLOR_LRGB2Lab (75)"},
        {CQpictureNamespace::COLOR_LBGR2Luv, "COLOR_LBGR2Luv (76)"},
        {CQpictureNamespace::COLOR_LRGB2Luv, "COLOR_LRGB2Luv (77)"},
        {CQpictureNamespace::COLOR_Lab2LBGR, "COLOR_Lab2LBGR (78)"},
        {CQpictureNamespace::COLOR_Lab2LRGB, "COLOR_Lab2LRGB (79)"},
        {CQpictureNamespace::COLOR_Luv2LBGR, "COLOR_Luv2LBGR (80)"},
        {CQpictureNamespace::COLOR_Luv2LRGB, "COLOR_Luv2LRGB (81)"},
        {CQpictureNamespace::COLOR_BGR2YUV, "COLOR_BGR2YUV (82)"},
        {CQpictureNamespace::COLOR_RGB2YUV, "COLOR_RGB2YUV (83)"},
        {CQpictureNamespace::COLOR_YUV2BGR, "COLOR_YUV2BGR (84)"},
        {CQpictureNamespace::COLOR_YUV2RGB, "COLOR_YUV2RGB (85)"},
        {CQpictureNamespace::COLOR_YUV2RGB_NV12, "COLOR_YUV2RGB_NV12 (90)"},
        {CQpictureNamespace::COLOR_YUV2BGR_NV12, "COLOR_YUV2BGR_NV12 (91)"},
        {CQpictureNamespace::COLOR_YUV2RGB_NV21, "COLOR_YUV2RGB_NV21 (92)"},
        {CQpictureNamespace::COLOR_YUV2BGR_NV21, "COLOR_YUV2BGR_NV21 (93)"},
        {CQpictureNamespace::COLOR_YUV420sp2RGB, "COLOR_YUV420sp2RGB (93)"},
        {CQpictureNamespace::COLOR_YUV420sp2BGR, "COLOR_YUV420sp2BGR (93)"},
        {CQpictureNamespace::COLOR_YUV2RGBA_NV12, "COLOR_YUV2RGBA_NV12 (94)"},
        {CQpictureNamespace::COLOR_YUV2BGRA_NV12, "COLOR_YUV2BGRA_NV12 (95)"},
        {CQpictureNamespace::COLOR_YUV2RGBA_NV21, "COLOR_YUV2RGBA_NV21 (96)"},
        {CQpictureNamespace::COLOR_YUV2BGRA_NV21, "COLOR_YUV2BGRA_NV21 (97)"},
        {CQpictureNamespace::COLOR_YUV420sp2RGBA, "COLOR_YUV420sp2RGBA (96)"},
        {CQpictureNamespace::COLOR_YUV420sp2BGRA, "COLOR_YUV420sp2BGRA (97)"},
        {CQpictureNamespace::COLOR_YUV2RGB_YV12, "COLOR_YUV2RGB_YV12 (98)"},
        {CQpictureNamespace::COLOR_YUV2BGR_YV12, "COLOR_YUV2BGR_YV12 (99)"},
        {CQpictureNamespace::COLOR_YUV2RGB_IYUV, "COLOR_YUV2RGB_IYUV (100)"},
        {CQpictureNamespace::COLOR_YUV2BGR_IYUV, "COLOR_YUV2BGR_IYUV (101)"},
        {CQpictureNamespace::COLOR_YUV2RGB_I420, "COLOR_YUV2RGB_I420 (100)"},
        {CQpictureNamespace::COLOR_YUV2BGR_I420, "COLOR_YUV2BGR_I420 (101)"},
        {CQpictureNamespace::COLOR_YUV420p2RGB, "COLOR_YUV420p2RGB (98)"},
        {CQpictureNamespace::COLOR_YUV420p2BGR, "COLOR_YUV420p2BGR (99)"},
        {CQpictureNamespace::COLOR_YUV2RGBA_YV12, "COLOR_YUV2RGBA_YV12 (102)"},
        {CQpictureNamespace::COLOR_YUV2BGRA_YV12, "COLOR_YUV2BGRA_YV12 (103)"},
        {CQpictureNamespace::COLOR_YUV2RGBA_IYUV, "COLOR_YUV2RGBA_IYUV (104)"},
        {CQpictureNamespace::COLOR_YUV2BGRA_IYUV, "COLOR_YUV2BGRA_IYUV (105)"},
        {CQpictureNamespace::COLOR_YUV2RGBA_I420, "COLOR_YUV2RGBA_I420 (104)"},
        {CQpictureNamespace::COLOR_YUV2BGRA_I420, "COLOR_YUV2BGRA_I420 (105)"},
        {CQpictureNamespace::COLOR_YUV420p2RGBA, "COLOR_YUV420p2RGBA (102)"},
        {CQpictureNamespace::COLOR_YUV420p2BGR, "COLOR_YUV420p2BGR (103)"},
        {CQpictureNamespace::COLOR_YUV2GRAY_420, "COLOR_YUV2GRAY_420 (106)"},
        {CQpictureNamespace::COLOR_YUV2GRAY_NV21, "COLOR_YUV2GRAY_NV21 (106)"},
        {CQpictureNamespace::COLOR_YUV2GRAY_NV12, "COLOR_YUV2GRAY_NV12 (106)"},
        {CQpictureNamespace::COLOR_YUV2GRAY_YV12, "COLOR_YUV2GRAY_YV12 (106)"},
        {CQpictureNamespace::COLOR_YUV2GRAY_IYUV , "COLOR_YUV2GRAY_IYUV (106)"},
        {CQpictureNamespace::COLOR_YUV2GRAY_I420, "COLOR_YUV2GRAY_I420 (106)"},
        {CQpictureNamespace::COLOR_YUV420sp2GRAY, "COLOR_YUV420sp2GRAY (106)"},
        {CQpictureNamespace::COLOR_YUV420p2GRAY , "COLOR_YUV420p2GRAY (106)"},
        {CQpictureNamespace::COLOR_YUV2RGB_UYVY, "COLOR_YUV2RGB_UYVY (107)"},
        {CQpictureNamespace::COLOR_YUV2BGR_UYVY, "COLOR_YUV2BGR_UYVY (108)"},
        {CQpictureNamespace::COLOR_YUV2RGB_Y422, "COLOR_YUV2RGB_Y422 (107)"},
        {CQpictureNamespace::COLOR_YUV2BGR_Y422, "COLOR_YUV2BGR_Y422 (108)"},
        {CQpictureNamespace::COLOR_YUV2RGB_UYNV, "COLOR_YUV2RGB_UYNV (107)"},
        {CQpictureNamespace::COLOR_YUV2BGR_UYNV, "COLOR_YUV2BGR_UYNV (108)"},
        {CQpictureNamespace::COLOR_YUV2RGBA_UYVY, "COLOR_YUV2RGBA_UYVY (111)"},
        {CQpictureNamespace::COLOR_YUV2BGRA_UYVY, "COLOR_YUV2BGRA_UYVY (112)"},
        {CQpictureNamespace::COLOR_YUV2RGBA_Y422, "COLOR_YUV2RGBA_Y422 (111)"},
        {CQpictureNamespace::COLOR_YUV2BGRA_Y422, "COLOR_YUV2BGRA_Y422 (112)"},
        {CQpictureNamespace::COLOR_YUV2RGBA_UYNV, "COLOR_YUV2RGBA_UYNV (111)"},
        {CQpictureNamespace::COLOR_YUV2BGRA_UYNV, "COLOR_YUV2BGRA_UYNV (112)"},
        {CQpictureNamespace::COLOR_YUV2RGB_YUY2, "COLOR_YUV2RGB_YUY2 (115)"},
        {CQpictureNamespace::COLOR_YUV2BGR_YUY2, "COLOR_YUV2BGR_YUY2 (116)"},
        {CQpictureNamespace::COLOR_YUV2RGB_YVYU, "COLOR_YUV2RGB_YVYU (117)"},
        {CQpictureNamespace::COLOR_YUV2BGR_YVYU, "COLOR_YUV2BGR_YVYU (118)"},
        {CQpictureNamespace::COLOR_YUV2RGB_YUYV, "COLOR_YUV2RGB_YUYV (115)"},
        {CQpictureNamespace::COLOR_YUV2BGR_YUYV, "COLOR_YUV2BGR_YUYV (116)"},
        {CQpictureNamespace::COLOR_YUV2RGB_YUNV, "COLOR_YUV2RGB_YUNV (115)"},
        {CQpictureNamespace::COLOR_YUV2BGR_YUNV, "COLOR_YUV2BGR_YUNV (116)"},
        {CQpictureNamespace::COLOR_YUV2RGBA_YUY2, "COLOR_YUV2RGBA_YUY2 (119)"},
        {CQpictureNamespace::COLOR_YUV2BGRA_YUY2, "COLOR_YUV2BGRA_YUY2 (120)"},
        {CQpictureNamespace::COLOR_YUV2RGBA_YVYU, "COLOR_YUV2RGBA_YVYU (121)"},
        {CQpictureNamespace::COLOR_YUV2BGRA_YVYU, "COLOR_YUV2BGRA_YVYU (122)"},
        {CQpictureNamespace::COLOR_YUV2RGBA_YUYV, "COLOR_YUV2RGBA_YUYV (119)"},
        {CQpictureNamespace::COLOR_YUV2BGRA_YUYV, "COLOR_YUV2BGRA_YUYV (120)"},
        {CQpictureNamespace::COLOR_YUV2RGBA_YUNV, "COLOR_YUV2RGBA_YUNV (119)"},
        {CQpictureNamespace::COLOR_YUV2BGRA_YUNV, "COLOR_YUV2BGRA_YUNV (120)"},
        {CQpictureNamespace::COLOR_YUV2GRAY_UYVY, "COLOR_YUV2GRAY_UYVY (123)"},
        {CQpictureNamespace::COLOR_YUV2GRAY_YUY2, "COLOR_YUV2GRAY_YUY2 (124)"},
        {CQpictureNamespace::COLOR_YUV2GRAY_Y422, "COLOR_YUV2GRAY_Y422 (123)"},
        {CQpictureNamespace::COLOR_YUV2GRAY_UYNV, "COLOR_YUV2GRAY_UYNV (123)"},
        {CQpictureNamespace::COLOR_YUV2GRAY_YVYU, "COLOR_YUV2GRAY_YVYU (124)"},
        {CQpictureNamespace::COLOR_YUV2GRAY_YUYV, "COLOR_YUV2GRAY_YUYV (124)"},
        {CQpictureNamespace::COLOR_YUV2GRAY_YUNV , "COLOR_YUV2GRAY_YUNV (124)"},
        {CQpictureNamespace::COLOR_RGBA2mRGBA, "COLOR_RGBA2mRGBA (125)"},
        {CQpictureNamespace::COLOR_mRGBA2RGBA, "COLOR_mRGBA2RGBA (126)"},
        {CQpictureNamespace::COLOR_RGB2YUV_I420, "COLOR_RGB2YUV_I420 (127)"},
        {CQpictureNamespace::COLOR_BGR2YUV_I420, "COLOR_BGR2YUV_I420 (128)"},
        {CQpictureNamespace::COLOR_RGB2YUV_IYUV, "COLOR_RGB2YUV_IYUV (127)"},
        {CQpictureNamespace::COLOR_BGR2YUV_IYUV, "COLOR_BGR2YUV_IYUV (128)"},
        {CQpictureNamespace::COLOR_RGBA2YUV_I420, "COLOR_RGBA2YUV_I420 (129)"},
        {CQpictureNamespace::COLOR_BGRA2YUV_I420, "COLOR_BGRA2YUV_I420 (130)"},
        {CQpictureNamespace::COLOR_RGBA2YUV_IYUV, "COLOR_RGBA2YUV_IYUV (129)"},
        {CQpictureNamespace::COLOR_BGRA2YUV_IYUV, "COLOR_BGRA2YUV_IYUV (130)"},
        {CQpictureNamespace::COLOR_RGB2YUV_YV12, "COLOR_RGB2YUV_YV12 (131)"},
        {CQpictureNamespace::COLOR_BGR2YUV_YV12, "COLOR_BGR2YUV_YV12 (132)"},
        {CQpictureNamespace::COLOR_RGBA2YUV_YV12, "COLOR_RGBA2YUV_YV12 (133)"},
        {CQpictureNamespace::COLOR_BGRA2YUV_YV12, "COLOR_BGRA2YUV_YV12 (134)"},
        {CQpictureNamespace::COLOR_BayerBG2BGR, "COLOR_BayerBG2BGR (46)"},
        {CQpictureNamespace::COLOR_BayerGB2BGR, "COLOR_BayerGB2BGR (47)"},
        {CQpictureNamespace::COLOR_BayerRG2BGR, "COLOR_BayerRG2BGR (48)"},
        {CQpictureNamespace::COLOR_BayerGR2BGR, "COLOR_BayerGR2BGR (49)"},
        {CQpictureNamespace::COLOR_BayerBG2RGB, "COLOR_BayerBG2RGB (48)"},
        {CQpictureNamespace::COLOR_BayerGB2RGB, "COLOR_BayerGB2RGB (49)"},
        {CQpictureNamespace::COLOR_BayerRG2RGB, "COLOR_BayerRG2RGB (46)"},
        {CQpictureNamespace::COLOR_BayerGR2RGB, "COLOR_BayerGR2RGB (47)"},
        {CQpictureNamespace::COLOR_BayerBG2GRAY, "COLOR_BayerBG2GRAY (86)"},
        {CQpictureNamespace::COLOR_BayerGB2GRAY, "COLOR_BayerGB2GRAY (87)"},
        {CQpictureNamespace::COLOR_BayerRG2GRAY, "COLOR_BayerRG2GRAY (88)"},
        {CQpictureNamespace::COLOR_BayerGR2GRAY, "COLOR_BayerGR2GRAY (89)"},
        {CQpictureNamespace::COLOR_BayerBG2BGR_VNG, "COLOR_BayerBG2BGR_VNG (62)"},
        {CQpictureNamespace::COLOR_BayerGB2BGR_VNG, "COLOR_BayerGB2BGR_VNG (63)"},
        {CQpictureNamespace::COLOR_BayerRG2BGR_VNG, "COLOR_BayerRG2BGR_VNG (64)"},
        {CQpictureNamespace::COLOR_BayerGR2BGR_VNG, "COLOR_BayerGR2BGR_VNG (65)"},
        {CQpictureNamespace::COLOR_BayerBG2RGB_VNG, "COLOR_BayerBG2RGB_VNG (64)"},
        {CQpictureNamespace::COLOR_BayerGB2RGB_VNG, "COLOR_BayerGB2RGB_VNG (65)"},
        {CQpictureNamespace::COLOR_BayerRG2RGB_VNG, "COLOR_BayerRG2RGB_VNG (62)"},
        {CQpictureNamespace::COLOR_BayerGR2RGB_VNG, "COLOR_BayerGR2RGB_VNG (63)"},
        {CQpictureNamespace::COLOR_BayerBG2BGR_EA, "COLOR_BayerBG2BGR_EA (135)"},
        {CQpictureNamespace::COLOR_BayerGB2BGR_EA, "COLOR_BayerGB2BGR_EA (136)"},
        {CQpictureNamespace::COLOR_BayerRG2BGR_EA, "COLOR_BayerRG2BGR_EA (137)"},
        {CQpictureNamespace::COLOR_BayerGR2BGR_EA, "COLOR_BayerGR2BGR_EA (138)"},
        {CQpictureNamespace::COLOR_BayerBG2RGB_EA, "COLOR_BayerBG2RGB_EA (137)"},
        {CQpictureNamespace::COLOR_BayerGB2RGB_EA, "COLOR_BayerGB2RGB_EA (138)"},
        {CQpictureNamespace::COLOR_BayerRG2RGB_EA, "COLOR_BayerRG2RGB_EA (115)"},
        {CQpictureNamespace::COLOR_BayerGR2RGB_EA, "COLOR_BayerGR2RGB_EA (136)"},
        {CQpictureNamespace::COLOR_COLORCVT_MAX, "COLOR_COLORCVT_MAX (139)"}
    };
}

CMainWindow::CMainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::CMainWindow)
{
    bool leftShowRes = false, rightShowRes = false;
    CQpictureNamespace::statusCodes leftLoadRes, rightLoadRes;
    int cnt;
    int depth, chNum, type;

    ui->setupUi(this);

    // Install event filter
    qApp->installEventFilter(this);

    // Check widgets
    CheckWidgets ();   

    // Make invisible what shall be invisible
    ui->lineEdit_subtractSrc1->setVisible (false);
    ui->lineEdit_subtractSrc2->setVisible (false);

    // Set frame
    ui->frame->setStyleSheet("border: 2px grey; border-radius: 4px;");
    ui->frame_processedImage->setStyleSheet("border: 2px solid red; border-radius: 4px;");

    // Set title
    QString tmpTitle = "Image Transformation" + tr(APP_TITLE_VERSION);
    this->setWindowTitle (tmpTitle);

    // Fix the size (make this not resizable)
    this->setFixedSize (this->geometry ().width (), this->geometry ().height ());

    // Hide what needs to be hided
    ui->label_colorConversionValue->setVisible(false);
    ui->lineEdit_colorConversionValue->setVisible(false);

    // Create a cqpicture object for each image to be displayed
    picture_left = new CQpicture ();
    picture_right = new CQpicture ();

    // Set selected picture to 'picture_processed', this image is the one selected at startup
    currentlySelectedPicture = picture_right;
    currentlySelectedCameraWidget = ui->cameraWidget_right;
    currentlySelectedImageLabel = ui->label_rightImage;

    // Set validator for QLineEdit controls
    ui->lineEdit_thresholdValue->setValidator (new QIntValidator (0, 255, this));
    ui->lineEdit_thresholdMax->setValidator (new QIntValidator (0, 255, this));
    ui->lineEdit_colorConversionValue->setValidator (new QIntValidator (0, 135, this));

    // Set slider to minimum number of points
    ui->verticalSlider_history->setMinimum (0);

    // Load dynamically color conversion codes
    cnt = 0;
    do
    {
        ui->comboBox_colorConversionDepth->addItem(colorConversionData[cnt].text);
    } while (colorConversionData[++cnt].code != CQpictureNamespace::COLOR_COLORCVT_MAX);

    // Load settings
    LoadSettings ();

    // Restore widgets status
    widgetSettings = new CQsettings ("Our", "Image Transformation");
    widgetSettings->readSettings (this);

    // Load image
    leftLoadRes = picture_left->LoadNewImage (pictureName_left);
    rightLoadRes = picture_right->LoadNewImage (pictureName_right);

    // Display images, to dispaly only the current selecte done use 'DisplayImage ()'
    if (CQpictureNamespace::STATUS_NO_ERROR == leftLoadRes)
    {
        picture_left->GetLoadedImage (mOrigImage);
        leftShowRes = ui->cameraWidget_left->showImage (mOrigImage);

        // Wait (mandatory to correctly display the image in some case)
        cv::waitKey (0);
    }

    // Show processed image
    if (CQpictureNamespace::STATUS_NO_ERROR == rightLoadRes)
    {
        picture_right->GetProcessedImage (mOrigImage);
        rightShowRes = ui->cameraWidget_right->showImage (mOrigImage);

        // Wait (mandatory to correctly display the image in some case)
        cv::waitKey (0);
    }

    if (false == leftShowRes && false == rightShowRes)
    {
        StatusBarMessage (QString ("Both images failed to load"));
    }
    else if (false == leftShowRes)
    {
        StatusBarMessage (QString ("Left image failed to laod"));
    }
    else if (false == rightShowRes)
    {
        StatusBarMessage (QString ("Right image failed to load"));
    }

    // Size slider to the history size (count right image because is the one defaulted to active)
    if (true == rightShowRes)
    {        
        // Update label
        picture_right->GetLoadedImageData (depth, chNum, type);

        QString tmpStr = QString ("Image loaded: depth=%1 - channels=%2 - type=%3").arg(QString::number(depth)).arg(QString::number(chNum)).arg(QString::number (type));
        ui->label_rightImage->setText(tmpStr);

        // Update history
        UpdateHistoryItems ();
    }

    if (true == leftShowRes)
    {
        // Update label
        picture_left->GetLoadedImageData (depth, chNum, type);

        QString tmpStr = QString ("Image loaded: depth=%1 - channels=%2 - type=%3").arg(QString::number(depth)).arg(QString::number(chNum)).arg(QString::number (type));
        ui->label_leftImage->setText(tmpStr);
    }
}

CMainWindow::~CMainWindow()
{
    // Save settings
    SaveSettings ();

    // Save widget status
    widgetSettings->writeSettings (this);

    delete ui;
}

void CMainWindow::CheckWidgets (void)
{
    // Find all QCheckBox and all QRadioButton
    QWidget *w = this;

    while (w)
    {
        if (CQtOpenCVViewerGl *openCvViewerGl = qobject_cast<CQtOpenCVViewerGl*>(w))
        {
            QString tmpStr = openCvViewerGl->objectName();

            qDebug () << QString("CQtOpenCVViewerGl %1").arg (tmpStr);

            if ("cameraWidget" == tmpStr)
            {
                openCvViewerGl->rect().getCoords(&coordinates_left.xTopLeft, &coordinates_left.yTopLeft,
                                                 &coordinates_left.xBottomRight, &coordinates_left.yBottomRigth);
            }
            else if ("cameraWidget_processed" == tmpStr)
            {
                openCvViewerGl->rect().getCoords(&coordinates_right.xTopLeft, &coordinates_right.yTopLeft,
                                                 &coordinates_right.xBottomRight, &coordinates_right.yBottomRigth);
            }
            else if ("cameraWidget_history" == tmpStr)
            {
                openCvViewerGl->rect().getCoords(&coordinates_history.xTopLeft, &coordinates_history.yTopLeft,
                                                 &coordinates_history.xBottomRight, &coordinates_history.yBottomRigth);
            }
        }

        if (QCheckBox *cb = qobject_cast<QCheckBox*>(w))
        {
            // It's a "QCheckBox":
            // re-style to accomodate bigger screen fontsStyling indicator
            cb->setStyleSheet ("QCheckBox::indicator { width: 14px; height: 14px; }");
        }

        // Go to next widget
        w = w->nextInFocusChain();

        if (w == this)
        {
            break;
        }
    }
}

bool CMainWindow::eventFilter(QObject *obj, QEvent *event)
{
    if (event->type() == QEvent::MouseButtonDblClick)
    {
        if (CQtOpenCVViewerGl *openCvViewerGl = qobject_cast<CQtOpenCVViewerGl*>(obj))
        {
            QString tmpStr = openCvViewerGl->objectName();

            if ("cameraWidget_left" == tmpStr)
            {
                // Set frame
                ui->frame->setStyleSheet("border: 2px solid red; border-radius: 4px;");
                ui->frame_processedImage->setStyleSheet("border: 2px grey; border-radius: 4px;");

                // Set currently selected picture, camera widget and label
                currentlySelectedPicture = picture_left;
                currentlySelectedCameraWidget = ui->cameraWidget_left;
                currentlySelectedImageLabel = ui->label_leftImage;

                // Correct subtract destination
                if (1 == ui->comboBox_subtractResult->currentIndex())
                {
                    ui->comboBox_subtractResult->setCurrentIndex (0);
                }

                // Update history
                UpdateHistoryItems ();

                qDebug () << "Picture selected is: 'picture'";
            }
            else if ("cameraWidget_right" == tmpStr)
            {
                // Set frame
                ui->frame->setStyleSheet("border: 2px grey; border-radius: 4px;");
                ui->frame_processedImage->setStyleSheet("border: 2px solid red; border-radius: 4px;");

                // Set currently selected picture, camera widget and label
                currentlySelectedPicture = picture_right;
                currentlySelectedCameraWidget = ui->cameraWidget_right;
                currentlySelectedImageLabel = ui->label_rightImage;

                // Correct subtract destination
                if (0 == ui->comboBox_subtractResult->currentIndex())
                {
                    ui->comboBox_subtractResult->setCurrentIndex (1);
                }

                // Update history
                UpdateHistoryItems ();

                qDebug () << "Picture selected is: 'picture_processed'";
            }
        }
    }

  return false;
}

void CMainWindow::UpdateCurrentImageLabel (QString str)
{
    int depth, chNum, type;

    // Set label
    currentlySelectedPicture->GetProcessedImageData (depth, chNum, type);

    QString tmpStr = QString ("%1 applied: depth=%2 - channels=%3 - type=%4").arg (str).arg (QString::number (depth)).arg (QString::number (chNum)).arg(QString::number (type));

    currentlySelectedImageLabel->setText (tmpStr);
}

void CMainWindow::UpdateHistoryItems (void)
{
    CQpictureNamespace::statusCodes res;

    // Display history image (for the currently selected picture)
    res = currentlySelectedPicture->GetHistoryImageAt (0, mHistoryImage);

    if (CQpictureNamespace::STATUS_NO_ERROR == res)
    {
        ui->cameraWidget_history->showImage (mHistoryImage);

        // Wait (mandatory to correctly display the image in some case)
        cv::waitKey (0);

        // Size slider to the history size
        int historySize = currentlySelectedPicture->GetHistorySize ();
        historySize = -historySize + 1;

        ui->verticalSlider_history->setMinimum (historySize);
    }
    else
    {
        ui->verticalSlider_history->setMinimum (0);

        ui->cameraWidget_history->BlankImage ();
    }
}

void CMainWindow::DisplayImage (void)
{
    // Show image
    currentlySelectedPicture->GetProcessedImage (mElabImage);
    currentlySelectedCameraWidget->showImage (mElabImage);

    // Wait (mandatory to correctly display the image in some case)
    cv::waitKey (0);

    // Size slider to the hostory size
    UpdateHistoryItems ();
}

void CMainWindow::LoadSettings (void)
{
    CQfilemanager *settingFile = new CQfilemanager ();
    QString filename;
    QString lastPictureValue;
    QFile::FileError error;

    // Get the current directory and opens etting file
    filename = QDir::currentPath() + QString (APP_FILE_SETTING);

    error = settingFile->Open (filename, QFile::ReadOnly, true);

    // Load the last picture loaded from setting files
    if (QFile::NoError == error)
    {
        // Load left picture
        if (settingFile->LoadSetting (APP_SETTING_LAST_LEFT_PICTURE, lastPictureValue) == QFile::NoError)
        {
            qDebug() << "Left picture loaded from setting file : " << lastPictureValue;

            pictureName_left = lastPictureValue;
        }
        else
        {
            qDebug () << "Default picture loaded (failed to find proper entry inside settings file)";

            // Build image name
            QString imagePath = QDir::currentPath ();
            imagePath.append ("/images/lion.jpg");

            // Save image name
            pictureName_left = imagePath;
        }

        // Load right picture
        if (settingFile->LoadSetting (APP_SETTING_LAST_RIGHT_PICTURE, lastPictureValue) == QFile::NoError)
        {
            qDebug() << "Right picture loaded from setting file : " << lastPictureValue;

            pictureName_right = lastPictureValue;
        }
        else
        {
            qDebug () << "Default picture loaded (failed to find proper entry inside settings file)";

            // Build image name
            QString imagePath = QDir::currentPath ();
            imagePath.append ("/images/lion.jpg");

            // Save image name
            pictureName_right = imagePath;
        }
    }
    else
    {
        qDebug () << "Default picture loaded (failed to open settings file)";

        // Build image name
        QString imagePath = QDir::currentPath ();
        imagePath.append ("/images/lion.jpg");

        // Save image name
        pictureName_left = imagePath;
        pictureName_right = imagePath;
    }
}

void CMainWindow::SaveSettings (void)
{
    QFile::FileError error;
    QString filename;
    CQfilemanager *settingFile = new CQfilemanager ();

    // Get the current directory and opens etting file
    filename = QDir::currentPath() + QString (APP_FILE_SETTING);

    error = settingFile->Open (filename, QFile::WriteOnly, false);

    // If we succeeded to open the setting file we save the last opened picture
    if (error == QFile::NoError)
    {
        // Save last images
        settingFile->SaveSetting (APP_SETTING_LAST_LEFT_PICTURE, pictureName_left);
        settingFile->SaveSetting (APP_SETTING_LAST_RIGHT_PICTURE, pictureName_right);

        qDebug () << "Last pictures saved";

        settingFile->Close ();
    }
    else
    {
        qDebug () << "Default picture not saved (failed to open settings file for write)";
    }
}

void CMainWindow::on_actionStart_triggered()
{
    QString file;

    if( mFaceDetector.empty() )
    {
        file = tr("%1/%2").arg(QApplication::applicationDirPath()).arg(FEAT_FACE_FILE);
        if(!mFaceDetector.load( file.toLatin1().constData() ))
            qDebug() << tr("Cannot find %1").arg(file);
    }

    if( mEyeDetector.empty() )
    {
        file = tr("%1/%2").arg(QApplication::applicationDirPath()).arg(FEAT_EYE_FILE);
        if(!mEyeDetector.load( file.toLatin1().constData() ))
            qDebug() << tr("Cannot find %1").arg(file);
    }

    if( mNoseDetector.empty() )
    {
        file = tr("%1/%2").arg(QApplication::applicationDirPath()).arg(FEAT_NOSE_FILE);
        if(!mNoseDetector.load( file.toLatin1().constData() ))
            qDebug() << tr("Cannot find %1").arg(file);
    }

    if( mMouthDetector.empty() )
    {
        file = tr("%1/%2").arg(QApplication::applicationDirPath()).arg(FEAT_MOUTH_FILE);
        if(!mMouthDetector.load( file.toLatin1().constData() ))
            qDebug() << tr("Cannot find %1").arg(file);
    }

    if( !mCapture.isOpened() )
    {
        mCapture.open(0);
        mCameraEventId = startTimer( 50 );
    }
}

void CMainWindow::on_actionStop_triggered()
{
    killTimer( mCameraEventId );

    mCapture.release();
}

void CMainWindow::timerEvent(QTimerEvent *event)
{
    if( event->timerId() == mCameraEventId )
    {
        // Stop Timer to stop receiving data from camera during elaboration
        killTimer( mCameraEventId );

        mCapture >> mOrigImage;

        mOrigImage.copyTo( mElabImage );

        if( ui->checkBox_fullFace->isChecked() )
        {
            vector< cv::Rect > faceVec;

            float scaleFactor = 3.0f; // Change Scale Factor to change speed
            mFaceDetector.detectMultiScale( mOrigImage, faceVec, scaleFactor );
            //mFaceDetector.detectSingleScale() mOrigImage, rectVec );

            for( size_t i=0; i<faceVec.size(); i++ )
            {
#if (defined USE_OPENCV3)
                cv::rectangle( mElabImage, faceVec[i], cv::Scalar(255,0,0), 2 );
#else
                cv::rectangle( mElabImage, faceVec[i], CV_RGB(255,0,0), 2 );
#endif // #if (defined USE_OPENCV3)

                cv::Mat face = mOrigImage( faceVec[i] );
                //cv::imshow( "Face", face );

                // ---> Eye Detection
                if( ui->checkBox_eyes->isChecked() )
                {
                    vector< cv::Rect > eyeVec;

                    mEyeDetector.detectMultiScale( face, eyeVec );

                    for( size_t j=0; j<eyeVec.size(); j++ )
                    {
                        cv::Rect rect = eyeVec[j];
                        rect.x += faceVec[i].x;
                        rect.y += faceVec[i].y;

#if (defined USE_OPENCV3)
                        cv::rectangle( mElabImage, rect, cv::Scalar(0,255,0), 2 );
#else
                        cv::rectangle( mElabImage, rect, CV_RGB(0,255,0), 2 );
#endif // #if (defined USE_OPENCV3)
                    }
                }
                // <--- Eye Detection

                // ---> Nose Detection
                if( ui->checkBox_nose->isChecked() )
                {
                    vector< cv::Rect > noseVec;

                    mNoseDetector.detectMultiScale( face, noseVec, 3 );

                    for( size_t j=0; j<noseVec.size(); j++ )
                    {
                        cv::Rect rect = noseVec[j];
                        rect.x += faceVec[i].x;
                        rect.y += faceVec[i].y;

#if (defined USE_OPENCV3)
                        cv::rectangle( mElabImage, rect, cv::Scalar(0,0,255), 2 );
#else
                        cv::rectangle( mElabImage, rect, CV_RGB(0,0,255), 2 );
#endif // #if (defined USE_OPENCV3)
                    }
                }
                // <--- Nose Detection

                // ---> Mouth Detection
                // [Searched in the bottom half face]
                if( ui->checkBox_mouth->isChecked() )
                {
                    vector< cv::Rect > mouthVec;
                    cv::Rect halfRect = faceVec[i];
                    halfRect.height /= 2;
                    halfRect.y += halfRect.height;

                    cv::Mat halfFace = mOrigImage( halfRect );

                    mMouthDetector.detectMultiScale( halfFace, mouthVec, 3 );

                    for( size_t j=0; j<mouthVec.size(); j++ )
                    {
                        cv::Rect rect = mouthVec[j];
                        rect.x += halfRect.x;
                        rect.y += halfRect.y;

#if (defined USE_OPENCV3)
                        cv::rectangle( mElabImage, rect, cv::Scalar(255,255,255), 2 );
#else
                        cv::rectangle( mElabImage, rect, CV_RGB(255,255,255), 2 );
#endif // #if (defined USE_OPENCV3)
                    }
                }
                // <--- Mouth Detection
            }
        }

        ui->cameraWidget_left->showImage (mElabImage);

        // Timer reactivation
        mCameraEventId = startTimer( 0 );
    }
}

void CMainWindow::on_checkBox_fullFace_clicked()
{
    if( ui->checkBox_fullFace->isChecked() )
    {
        ui->checkBox_eyes->setEnabled( true );
        ui->checkBox_nose->setEnabled( true );
        ui->checkBox_mouth->setEnabled( true );
    }
    else
    {
        ui->checkBox_eyes->setEnabled( false );
        ui->checkBox_nose->setEnabled( false );
        ui->checkBox_mouth->setEnabled( false );
    }
}

void CMainWindow::on_pushButton_applyThreshold_clicked()
{
    int trasformType;
    int curIndex;
    int thrValue, maxValue;
    QString tmpStr;
    bool applyOtsu;
    CQpictureNamespace::statusCodes res;

    // Get data
    curIndex = ui->comboBox_thresholdType->currentIndex ();

    if ("BINARY" == ui->comboBox_thresholdType->itemText (curIndex))
    {
        trasformType = cv::THRESH_BINARY;
    }
    else if ("BINARY_INV" == ui->comboBox_thresholdType->itemText (curIndex))
    {
        trasformType = cv::THRESH_BINARY_INV;
    }
    else if ("TRUNC" == ui->comboBox_thresholdType->itemText (curIndex))
    {
        trasformType = cv::THRESH_TRUNC;
    }
    else if ("TOZERO" == ui->comboBox_thresholdType->itemText (curIndex))
    {
        trasformType = cv::THRESH_TOZERO;
    }
    else if ("TOZERO_INV" == ui->comboBox_thresholdType->itemText (curIndex))
    {
        trasformType = cv::THRESH_TOZERO_INV;
    }
    else
    {
        StatusBarMessage (QString ("Invalid 'trasformType' parameter"));

        return;
    }

    thrValue = ui->lineEdit_thresholdValue->text ().toInt ();
    maxValue = ui->lineEdit_thresholdMax->text ().toInt ();

    applyOtsu = ui->checkBox_thresholdOtsu->isChecked();

    tmpStr = "TYPE : " + ui->comboBox_thresholdType->itemText (curIndex) + " - " +
             "THR : " + ui->lineEdit_thresholdValue->text () + " - " +
             "MAX : " + ui->lineEdit_thresholdMax->text ();\

    currentlySelectedImageLabel->setText (tmpStr);

    // Apply transform
    res = currentlySelectedPicture->ApplyThreshold (thrValue, maxValue, trasformType, applyOtsu);

    if (CQpictureNamespace::STATUS_NO_ERROR == res)
    {
        // Display images
        DisplayImage ();

        // Set label
        UpdateCurrentImageLabel (tr("Threshold"));
    }
    else if (CQpictureNamespace::ERR_INVALID_IMAGE == res)
    {
        StatusBarMessage (QString ("Invalid image"));
    }
    else if (CQpictureNamespace::ERR_INVALID_IMAGE_TYPE == res)
    {
        StatusBarMessage (QString ("Invalid image type (most probably OTSU used with image type with more than 8 bits"));
    }
}

void CMainWindow::StatusBarMessage (QString msg)
{
    ui->statusBar->setStyleSheet("QStatusBar{padding-left:8px;color:red;font-weight:bold;}");

    ui->statusBar->showMessage (msg, 3000);
}

void CMainWindow::on_pushButton_applyGreyScale_clicked()
{
    int colorSpaceIndex, depth = -1;
    int channelsNumber;
    int cnt;
    CQpictureNamespace::statusCodes ret;

    // Get value for color space (it is a direct conversion of the index)
    colorSpaceIndex = ui->comboBox_colorConversionDepth->currentIndex();

    cnt = 0;
    while (colorConversionData[cnt].code != CQpictureNamespace::COLOR_COLORCVT_MAX)
    {
        if (ui->comboBox_colorConversionDepth->itemText(colorSpaceIndex) == colorConversionData[cnt].text)
        {
            depth = colorConversionData[cnt].code;

            break;
        }

        cnt++;
    };

    // Check that a valid value has been found
    if (-1 == depth)
    {
       StatusBarMessage (QString ("Failed to convert using cvColor : invalid conversion code entry"));
    }

    // Get value for channels number
    channelsNumber = ui->lineEdit_colorConversionNumChannels->text().toInt();

    // Process image
    ret = currentlySelectedPicture->ApplyColorSpaceConversion (depth, channelsNumber);

    // Check result and in case print text on status bar
    if (CQpictureNamespace::ERR_INVALID_IMAGE == ret)
    {
        StatusBarMessage (QString ("Failed to convert using cvColor : invalid image"));
    }
    else if (CQpictureNamespace::ERR_INVALID_CHANNEL_NUMBER == ret)
    {
        StatusBarMessage (QString ("Failed to convert using cvColor : invalid channels number"));
    }
    else if (CQpictureNamespace::ERR_INVALID_COLOR_CONV == ret)
    {
        StatusBarMessage (QString ("Failed to convert using cvColor : invalid color conversion"));
    }
    else
    {
        // Display images
        DisplayImage ();

        // Set label
        UpdateCurrentImageLabel (tr("Gray scale"));
    }
}

void CMainWindow::on_actionReset_triggered()
{
    int chNum, depth, type;

    // Reset used image for processing to originally loaded image
    currentlySelectedPicture->ResetProcessedImage ();

    // Show image
    currentlySelectedPicture->GetLoadedImage (mOrigImage);
    currentlySelectedCameraWidget->showImage (mOrigImage);

    // Wait (mandatory to correctly display the image in some case)
    cv::waitKey (0);

    // Update label
    currentlySelectedPicture->GetLoadedImageData (depth, chNum, type);

    QString tmpStr = QString ("Image loaded: depth=%1 - channles=%2 - type=%3").arg(QString::number(depth)).arg(QString::number(chNum)).arg(QString::number (type));
    currentlySelectedImageLabel->setText(tmpStr);

    // Size slider to the hostory size
    UpdateHistoryItems ();
}

void CMainWindow::on_actionLoad_Image_triggered()
{
    QString curPath = QDir::currentPath() + "/images";

    // Load image
    if (picture_left == currentlySelectedPicture)
    {
        pictureName_left = QFileDialog::getOpenFileName (this, tr("Open File"), curPath, tr ("Image Files (*.png *.jpg *.bmp)"));

        currentlySelectedPicture->LoadNewImage (pictureName_left);

        qDebug() << "Loaded image : " << pictureName_left;
    }
    else
    {
        pictureName_right = QFileDialog::getOpenFileName (this, tr("Open File"), curPath, tr ("Image Files (*.png *.jpg *.bmp)"));

        currentlySelectedPicture->LoadNewImage (pictureName_right);

        qDebug() << "Loaded image : " << pictureName_right;
    }

    // Display image
    DisplayImage ();

    // Set label
    UpdateCurrentImageLabel (tr("Image loaded"));
}

void CMainWindow::on_actionOpen_Howto_triggered()
{
    QDesktopServices::openUrl (QUrl("file:///" + QDir::currentPath() + "/docs/ImageTransformation.pdf", QUrl::TolerantMode));
}

void CMainWindow::on_actionSave_processed_image_triggered()
{
    QString picturePath;

    // Save processed image
    picturePath = QFileDialog::getSaveFileName (this, "Save image", QString(), "*.jpg");

    if (false == picturePath.isEmpty())
    {
        currentlySelectedPicture->SaveProcessedImage (picturePath);
    }
}

void CMainWindow::on_pushButton_applyMorphologyEx_clicked()
{
    int structElemShape, structElemSize, morphType, iterationsNum;
    CQpictureNamespace::statusCodes res;
    int curIndex;

    // Get data for structuring element
    curIndex = ui->comboBox_morphologyExStructElemShape->currentIndex ();

    if ("MORPH_RECT" == ui->comboBox_morphologyExStructElemShape->itemText (curIndex))
    {
        structElemShape = cv::MORPH_RECT;
    }
    else if ("MORPH_ELLIPSE" == ui->comboBox_morphologyExStructElemShape->itemText (curIndex))
    {
        structElemShape = cv::MORPH_ELLIPSE;
    }
    else if ("MORPH_CROSS" == ui->comboBox_morphologyExStructElemShape->itemText (curIndex))
    {
        structElemShape = cv::MORPH_CROSS;
    }
    else
    {
        StatusBarMessage (QString ("Invalid 'structElemShape' parameter"));

        return;
    }

    // Get data for morphology type
    curIndex = ui->comboBox_morphologyExType->currentIndex ();

    if ("MORPH_OPEN" == ui->comboBox_morphologyExType->itemText (curIndex))
    {
        morphType = cv::MORPH_RECT;
    }
    else if ("MORPH_CLOSE" == ui->comboBox_morphologyExType->itemText (curIndex))
    {
        morphType = cv::MORPH_ELLIPSE;
    }
    else if ("MORPH_GRADIENT" == ui->comboBox_morphologyExType->itemText (curIndex))
    {
        morphType = cv::MORPH_CROSS;
    }
    else if ("MORPH_TOPHAT" == ui->comboBox_morphologyExType->itemText (curIndex))
    {
        morphType = cv::MORPH_CROSS;
    }
    else if ("MORPH_BLACKHAT" == ui->comboBox_morphologyExType->itemText (curIndex))
    {
        morphType = cv::MORPH_CROSS;
    }
    else
    {
        StatusBarMessage (QString ("Invalid 'morphType' parameter"));

        return;
    }

    structElemSize = ui->lineEdit_morphologyExStructElemSize->text ().toInt ();
    iterationsNum = ui->lineEdit_morphologyExIterationNumber->text ().toInt ();

    res = currentlySelectedPicture->ApplyMorphologyEx (structElemShape, structElemSize, morphType, iterationsNum);

    if (CQpictureNamespace::STATUS_NO_ERROR != res)
    {
        StatusBarMessage (QString ("Failed to apply MorphologyEx"));
    }
    else
    {
        // Display images
        DisplayImage ();

        // Set label
        UpdateCurrentImageLabel (tr("MorphologyEx"));
    }
}

void CMainWindow::on_actionLoad_and_Execute_script_triggered()
{
    // Load script and pass it to the scripter for executing
}

void CMainWindow::on_pushButton_applyDilate_clicked()
{
    CQpictureNamespace::statusCodes res;
    int structElemShape, structElemSize, iterationsNum;
    int curIndex;

    // Get data for structuring element
    curIndex = ui->comboBox_dilateStructElemShape->currentIndex ();

    if ("MORPH_RECT" == ui->comboBox_dilateStructElemShape->itemText (curIndex))
    {
        structElemShape = cv::MORPH_RECT;
    }
    else if ("MORPH_ELLIPSE" == ui->comboBox_dilateStructElemShape->itemText (curIndex))
    {
        structElemShape = cv::MORPH_ELLIPSE;
    }
    else if ("MORPH_CROSS" == ui->comboBox_dilateStructElemShape->itemText (curIndex))
    {
        structElemShape = cv::MORPH_CROSS;
    }
    else
    {
        StatusBarMessage (QString ("Invalid 'structElemShape' parameter"));

        return;
    }

    structElemSize = ui->lineEdit_dilateStructElemSize->text ().toInt ();
    iterationsNum = ui->lineEdit_dilateIterationNumber->text ().toInt ();

    res = currentlySelectedPicture->ApplyDilate(structElemShape, structElemSize, iterationsNum);

    if (CQpictureNamespace::STATUS_NO_ERROR != res)
    {
        StatusBarMessage (QString ("Failed to apply Dilate"));
    }
    else
    {
        // Display images
        DisplayImage ();

        // Set label
        UpdateCurrentImageLabel (tr("Dilate"));
    }
}

void CMainWindow::on_pushButton_applyDistanceTransform_clicked()
{
    int curIndex;
    int transformLabelType;
    int transformMaxSize;
    int distanceType;
    CQpictureNamespace::statusCodes res;
    cv::Mat outputLabels2DArray;

    // Distance transform type: CV_DIST_L1, CV_DIST_L2 or CV_DIST_C
    curIndex = ui->comboBox_distanceTransformType->currentIndex ();

    if ("CV_DIST_L1" == ui->comboBox_distanceTransformType->itemText (curIndex))
    {
        distanceType = cv::DIST_L1;
    }
    else if ("CV_DIST_L2" == ui->comboBox_distanceTransformType->itemText (curIndex))
    {
        distanceType = cv::DIST_L2;
    }
    else if ("CV_DIST_C" == ui->comboBox_distanceTransformType->itemText (curIndex))
    {
        distanceType = cv::DIST_C;
    }
    else
    {
        StatusBarMessage (QString ("Invalid 'Distance' transform type parameter"));

        return;
    }

    // Mask size: 3, 5 or CV_DIST_MASK_PRECISE
    curIndex = ui->comboBox_distanceTransformMaskSize->currentIndex();

    if ("3 x 3" == ui->comboBox_distanceTransformMaskSize->itemText (curIndex))
    {
        transformMaxSize = cv::DIST_MASK_3;
    }
    else if ("5 x 5" == ui->comboBox_distanceTransformMaskSize->itemText (curIndex))
    {
        transformMaxSize = cv::DIST_MASK_5;
    }
    else if ("CV_DIST_MASK_PRECISE" == ui->comboBox_distanceTransformMaskSize->itemText (curIndex))
    {
        transformMaxSize = cv::DIST_MASK_PRECISE;
    }
    else
    {
        StatusBarMessage (QString ("Invalid 'Distance' transform mask size parameter"));

        return;
    }

    // Label type: DIST_LABEL_CCOMP, DIST_LABEL_PIXEL  or NOT USED
    curIndex = ui->comboBox_distanceTransformMaskSize->currentIndex();

    if ("3 x 3" == ui->comboBox_distanceTransformMaskSize->itemText (curIndex))
    {
        transformLabelType = cv::DIST_MASK_3;
    }
    else if ("5 x 5" == ui->comboBox_distanceTransformMaskSize->itemText (curIndex))
    {
        transformLabelType = cv::DIST_MASK_5;;
    }
    else if ("CV_DIST_MASK_PRECISE" == ui->comboBox_distanceTransformMaskSize->itemText (curIndex))
    {
        transformLabelType = cv::DIST_MASK_PRECISE;
    }
    else
    {
        StatusBarMessage (QString ("Invalid 'Distance' transform mask size parameter"));

        return;
    }

    res = currentlySelectedPicture->ApplyDistanceTransform (distanceType, outputLabels2DArray, transformMaxSize, transformLabelType);

    if (CQpictureNamespace::STATUS_NO_ERROR != res)
    {
        StatusBarMessage (QString ("Subtract operation failed"));
    }
    else
    {
        // Display images
        DisplayImage ();

        // Set label
        UpdateCurrentImageLabel (tr("Distance transform"));

        // TODO: do something with the output array
    }
}

void CMainWindow::on_pushButton_applySubtract_clicked()
{
    int curIndexSrc1, curIndexResultSlot;
    CQpictureNamespace::statusCodes res;
    cv::Mat tmpImage;

    // Get data for src elements
    curIndexSrc1 = ui->comboBox_subtractSrc1->currentIndex ();
    curIndexResultSlot = ui->comboBox_subtractResult->currentIndex ();

    if (curIndexSrc1 == curIndexResultSlot)
    {
        if (picture_left == currentlySelectedPicture)
        {
            picture_right->GetProcessedImage (tmpImage);

            res = currentlySelectedPicture->ApplySubtract (CQpictureNamespace::SUBTRACT_FROM, tmpImage);
        }
        else
        {
            picture_left->GetProcessedImage (tmpImage);

            res = currentlySelectedPicture->ApplySubtract (CQpictureNamespace::SUBTRACT_FROM, tmpImage);
        }
    }
    else
    {
        if (picture_right == currentlySelectedPicture)
        {
            picture_left->GetProcessedImage (tmpImage);

            res = currentlySelectedPicture->ApplySubtract (CQpictureNamespace::SUBTRACT_TO, tmpImage);
        }
        else
        {
            picture_right->GetProcessedImage (tmpImage);

            res = currentlySelectedPicture->ApplySubtract (CQpictureNamespace::SUBTRACT_TO, tmpImage);
        }
    }

    // Check result
    if (CQpictureNamespace::ERR_INVALID_VECTOR_SIZES == res)
    {
       StatusBarMessage (QString ("Subtract operation failed due to wrong vector sizes"));
    }
    else if (CQpictureNamespace::STATUS_NO_ERROR != res)
    {
        StatusBarMessage (QString ("Subtract operation failed"));
    }
    else
    {
        // Display images
        DisplayImage ();

        // Set label
        UpdateCurrentImageLabel (tr("Subtract"));
    }
}

void CMainWindow::on_comboBox_distanceTransformType_currentIndexChanged(const QString &arg1)
{
    if ("CV_DIST_L1" == arg1 || "CV_DIST_C" == arg1)
    {
        // In this case the transfor mask is set to second index  (5)
        ui->comboBox_distanceTransformMaskSize->setCurrentIndex (0);
        ui->comboBox_distanceTransformMaskSize->setEnabled (false);
    }
    else
    {
        ui->comboBox_distanceTransformMaskSize->setEnabled (true);
    }
}

void CMainWindow::on_comboBox_distanceTransformLabelType_currentIndexChanged(const QString &arg1)
{
    if ("NOT_USED" != arg1)
    {
       ui->comboBox_distanceTransformMaskSize->setMaxCount(2);
    }
    else
    {
        ui->comboBox_distanceTransformMaskSize->setMaxCount(3);
        ui->comboBox_distanceTransformMaskSize->addItem ("CV_DIST_MASK_PRECISE");
    }
}

void CMainWindow::on_verticalSlider_history_valueChanged(int value)
{
    CQpictureNamespace::statusCodes ret;

    // Display history image
    ret = currentlySelectedPicture->GetHistoryImageAt (value, mHistoryImage);

    if (CQpictureNamespace::ERR_INVALID_IMAGE == ret)
    {
        StatusBarMessage (QString ("Invalid image requested to history"));
    }
    else
    {
        ui->cameraWidget_history->showImage (mHistoryImage);

        // Wait (mandatory to correctly display the image in some case)
        cv::waitKey (0);
    }
}

void CMainWindow::on_comboBox_subtractSrc1_currentIndexChanged(int index)
{
    if (0 == index)
    {
        ui->comboBox_subtractSrc2->setCurrentIndex (1);

        ui->lineEdit_subtractSrc1->setVisible (false);
    }
    else if (1 == index)
    {
        ui->comboBox_subtractSrc2->setCurrentIndex (0);

        ui->lineEdit_subtractSrc1->setVisible (false);
    }
    else if (2 == index)
    {
        ui->lineEdit_subtractSrc1->setVisible (true);
    }
}

void CMainWindow::on_comboBox_subtractSrc2_currentIndexChanged(int index)
{
    if (0 == index)
    {
        ui->comboBox_subtractSrc1->setCurrentIndex (1);

        ui->lineEdit_subtractSrc2->setVisible (false);
    }
    else if (1 == index)
    {
        ui->comboBox_subtractSrc1->setCurrentIndex (0);

        ui->lineEdit_subtractSrc2->setVisible (false);
    }
    else if (2 == index)
    {
        ui->lineEdit_subtractSrc2->setVisible (true);
    }
}

void CMainWindow::on_comboBox_subtractResult_currentIndexChanged(int index)
{
    if ("LEFT IMAGE" == ui->comboBox_subtractResult->itemText (index))
    {
        // Set currently selected picture, camera widget and label
        currentlySelectedPicture = picture_left;
        currentlySelectedCameraWidget = ui->cameraWidget_left;
        currentlySelectedImageLabel = ui->label_leftImage;

        // Set frame
        ui->frame->setStyleSheet("border: 2px solid red; border-radius: 4px;");
        ui->frame_processedImage->setStyleSheet("border: 2px grey; border-radius: 4px;");

        // Update history
        UpdateHistoryItems ();
    }
    else
    {
        // Set currently selected picture, camera widget and label
        currentlySelectedPicture = picture_right;
        currentlySelectedCameraWidget = ui->cameraWidget_right;
        currentlySelectedImageLabel = ui->label_rightImage;

        // Set frame
        ui->frame->setStyleSheet("border: 2px grey; border-radius: 4px;");
        ui->frame_processedImage->setStyleSheet("border: 2px solid red; border-radius: 4px;");
    }
}

void CMainWindow::on_pushButton_applyWatershed_clicked()
{
    CQpictureNamespace::statusCodes res;
    int curIndex;
    cv::Mat tmpImage;

    // Get data for src elements
    curIndex = ui->comboBox_applyWatershedMarkerImage->currentIndex();

    if (LEFT_IMAGE_INDEX_VALUE == curIndex)
    {
        // Get the left image
        picture_left->GetProcessedImage (tmpImage);

        // Apply wateshed
        res = currentlySelectedPicture->ApplyWatershed(tmpImage);
    }
    else // RIGHT_IMAGE_INDEX_VALUE
    {
        // Get the left image
        picture_right->GetProcessedImage (tmpImage);

        // Apply wateshed
        res = currentlySelectedPicture->ApplyWatershed(tmpImage);
    }

    // Check result
    if (CQpictureNamespace::STATUS_NO_ERROR != res)
    {
        StatusBarMessage (QString ("Watershed operation failed"));
    }
    else
    {
        // Display images
        DisplayImage ();

        // Set label
        UpdateCurrentImageLabel (tr("Watershed"));
    }
}

void CMainWindow::on_pushButton_applyConnectedComponents_clicked()
{
    CQpictureNamespace::statusCodes res;
    int connectivity, ltype;
    int curIndex;

    // Get data for connectivity
    curIndex = ui->comboBox_connectedComponentsConnectivity->currentIndex ();

    if ("4-WAY" == ui->comboBox_connectedComponentsConnectivity->itemText (curIndex))
    {
        connectivity = 4;
    }
    else if ("8-WAY" == ui->comboBox_connectedComponentsConnectivity->itemText (curIndex))
    {
        connectivity = 8;
    }
    else
    {
        StatusBarMessage (QString ("Invalid 'connectivity' parameter"));

        return;
    }

    // Get data for ltype
    curIndex = ui->comboBox_connectedComponentsLType->currentIndex ();

    if ("CV_32S" == ui->comboBox_connectedComponentsLType->itemText (curIndex))
    {
        ltype = CV_32S;
    }
    else if ("CV_16U" == ui->comboBox_connectedComponentsLType->itemText (curIndex))
    {
        ltype = CV_16U;
    }
    else
    {
        StatusBarMessage (QString ("Invalid 'ltype' parameter"));

        return;
    }

    // Apply connected components
    res = currentlySelectedPicture->ApplyConnectedComponents (connectivity, ltype);

    // Check result
    if (CQpictureNamespace::STATUS_NO_ERROR != res)
    {
        StatusBarMessage (QString ("Connect Components operation failed"));
    }
    else
    {
        // Display images
        DisplayImage ();

        // Set label
        UpdateCurrentImageLabel (tr("Connect Components"));
    }
}

void CMainWindow::on_pushButton_getDetailedError_clicked()
{
    QString res;

    res = currentlySelectedPicture->GetDetailedError ();

    ui->textEdit_detailedError->setText(res);
}

void CMainWindow::on_actionWatersheed_triggered()
{

}
