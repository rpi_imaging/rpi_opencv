#include <QCheckBox>
#include <QComboBox>
#include <QLineEdit>

#include "cqsettings.h"

CQsettings::CQsettings(QString app, QString name, QString siteAdr, QWidget *parent) : QWidget(parent)
{
    appName = app;
    companyName = name;
    companySite = siteAdr;
}

CQsettings::~CQsettings()
{

}

void CQsettings::readSettings(QWidget* window)
{
    QSettings settings;

    settings.beginGroup(window->objectName());

    QVariant value = settings.value("pos");

    if (!value.isNull())
    {
        window->move(settings.value("pos").toPoint());
        window->resize(settings.value("size").toSize());
        recurseRead(settings, window);
    }

    settings.endGroup();
}

void CQsettings::writeSettings(QWidget* window)
{
    QSettings settings;

    settings.beginGroup(window->objectName());

    settings.setValue("pos", window->pos());
    settings.setValue("size", window->size());

    recurseWrite(settings, window);

    settings.endGroup();
}

void CQsettings::recurseRead(QSettings& settings, QObject* object)
{
    // Check for check box items
    QCheckBox* checkbox = dynamic_cast<QCheckBox*>(object);

    if (0 != checkbox)
    {
        checkbox->setChecked(settings.value(checkbox->objectName()).toBool());
    }

    // Check got combo box items
    QComboBox* combobox = dynamic_cast<QComboBox*>(object);


    if (0 != combobox)
    {
        int tmpIndex = settings.value(combobox->objectName()).toInt();

        if (tmpIndex < combobox->maxCount() && combobox->isEnabled())
        {
            combobox->setCurrentIndex(tmpIndex);
        }
    }

    // Line edit items
    QLineEdit* lineEdit = dynamic_cast<QLineEdit *>(object);

    if (0 != lineEdit)
    {
        QString tmpStr = settings.value(lineEdit->objectName()).toString();

        if (tmpStr.length() > 0)
        {
            lineEdit->setText (tmpStr);
        }
    }

    // Recurse for childs
    foreach(QObject* child, object->children())
    {
        recurseRead(settings, child);
    }
}

void CQsettings::recurseWrite(QSettings& settings, QObject* object)
{
    // Check for check box items
    QCheckBox* checkbox = dynamic_cast<QCheckBox*>(object);

    if (0 != checkbox)
    {
        settings.setValue(checkbox->objectName(), checkbox->isChecked());
    }

    // Check got combo box items
    QComboBox* combobox = dynamic_cast<QComboBox*>(object);

    if (0 != combobox)
    {
        settings.setValue(combobox->objectName(), combobox->currentIndex());
    }

    // Line edit items
    QLineEdit* lineEdit = dynamic_cast<QLineEdit *>(object);

    if (0 != lineEdit)
    {
        settings.setValue(lineEdit->objectName(), lineEdit->text());
    }

    // Recurse for childs
    foreach(QObject* child, object->children())
    {
        recurseWrite(settings, child);
    }
}

